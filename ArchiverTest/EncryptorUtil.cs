using System.Security.Cryptography;
using System.Text;
using Xunit.Abstractions;

namespace ArchiverTest
{
    public class EncryptorUtil
    {
        private readonly ITestOutputHelper _output;

        public EncryptorUtil(ITestOutputHelper output)
        {
            _output = output;
        }
        
        public static string EncryptString(string plainText, byte[] key, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(plainText);
                        }
                        return Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
        }

        [Fact(Skip = "Only run this locally.")]
        public void EncryptItem()
        {
            string textToEncrypt = "test";
            string key = "";
            string iv = "";

            _output.WriteLine($"Encrypted text result: {EncryptString(textToEncrypt, Encoding.Default.GetBytes(key), Encoding.Default.GetBytes(iv))}");
        }
    }
}