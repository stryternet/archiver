using System.Reflection;
using Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts;

namespace ArchiverTest.Managers.DatabaseManger.DatabaseParts
{


    public class RootPathDatabaseDefinitionTest
    {
        #region FromRootPathAndFiles Tests

        [Fact]
        public async Task FromRootPathAndFiles_Test1()
        {
            string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(location, @"Examples/");

            RootPathDatabaseDefinition toAssert = await RootPathDatabaseDefinition.FromRootPathAndFiles(path, "/Playground/temp", ["temp.001.zip.adb"], default);

            Assert.NotNull(toAssert);
            Assert.Equal("/Playground/temp", toAssert.RootPath);
            Assert.Single(toAssert.ArchiveDatabaseFiles);
        }


        #endregion
    }
}