using System.Reflection;
using Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts;

namespace ArchiverTest.Managers.DatabaseManger.DatabaseParts;

public class ArchiveDatabaseFileTest
{
    #region FromPathAndName Tests

    [Fact]
    public async Task FromPathAndName_Test1()
    {
        string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        string path = Path.Combine(location, @"Examples/");
        ArchiveDatabaseFile toAssert = await ArchiveDatabaseFile.FromPathAndName(path, "archive.adb", default);

        Assert.NotNull(toAssert);

        Assert.Equal(3, toAssert.RootPaths.Count);

        Assert.Equal("archive.adb", toAssert.Name);

        Assert.Equal(path, toAssert.PathToFile);

        Assert.Equal(3, toAssert.RootPathDatabaseDefinitions.Count);

        foreach (RootPathDatabaseDefinition item in toAssert.RootPathDatabaseDefinitions)
        {
            Assert.Contains(item.RootPath, toAssert.RootPaths);
        }
    }

    #endregion
}