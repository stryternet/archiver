using System.Reflection;
using Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts;

namespace ArchiverTest.Managers.DatabaseManger.DatabaseParts
{


    public class ZipArchiveDatabaseFileTest
    {
        #region FromPathAndName Tests
        
        [Fact]
        public async Task FromPathAndName_Test1()
        {
            string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(location, @"Examples/");
            ZipArchiveDatabaseFile toAssert = await ZipArchiveDatabaseFile.FromPathAndName(path, "temp.001.zip.adb", default);

            Assert.NotNull(toAssert);

            Assert.Equal("temp.001.zip.adb", toAssert.Name);
            Assert.Equal(path, toAssert.PathToFile);
            Assert.Equal(0, toAssert.SpaceRemainingInBytes);
            Assert.Equal(3, toAssert.Files.Count);
            Assert.Contains(@"\900MiB.bin", toAssert.Files);
            Assert.Contains(@"\968MiB.txt", toAssert.Files);
            Assert.Contains(@"\PXL_20240616_024648197_3.mp4", toAssert.Files);
        }


        #endregion
    }
}