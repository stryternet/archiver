using Archiver.Clients;

namespace ArchiverTest.Clients
{
    public class B2ClientTests
    {
        [Fact(Skip = "Only run this locally")]
        public async Task TestClient()
        {
            B2Client cut = new("00572253139685c0000000003", "GET THIS FROM THE CONFIG");

            B2Client.B2ListFileVersionsResponse response = await cut.ListFileVersionsAsync("57a2b2a5035193299618051c", default);

            Assert.NotNull(response);
        }
    }
}