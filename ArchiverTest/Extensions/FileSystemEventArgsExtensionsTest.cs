using System.Reflection;
using Archiver.Extensions;

namespace ArchiverTest.Extensions
{
    public class FileSystemEventArgsExtensionsTest
    {

        #region ToHashKey

        [Theory]
        [InlineData("Examples/archive.adb", "Changed::Examples/archive.adb::$/Examples/archive.adb")]
        public void ToHashKey(string pathToTest, string expectedKey)
        {
            string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(location, pathToTest);

            PollingFileSystemEventArgs args = new(PollingFileSystemEventType.Changed, path, Path.GetDirectoryName(pathToTest));
            string actual = args.ToHashKey();
            Assert.Equal(expectedKey.Replace("$", location), actual);
        }

        #endregion
    }
}