using System.Collections;
using System.IO.Compression;
using System.Text;
using Archiver;
using Archiver.Clients;
using Archiver.Config;
using Archiver.Helpers;
using Archiver.Managers.DatabaseManager;
using Archiver.Managers.DatabaseManager.MultiFileDatabaseManager;
using Archiver.Utilities;
using ArchiverService;

// Get the application builder
HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

// Grab the config for the service and create a proper class instance
IConfigurationSection config = builder.Configuration.GetSection("Archiver");
builder.Services.Configure<ArchiverConfig>(config);
IConfigurationSection dbConfig = builder.Configuration.GetSection("Database");
builder.Services.Configure<DatabaseConfig>(dbConfig);
IConfigurationSection storageConfig = builder.Configuration.GetSection("Storage");
builder.Services.Configure<StorageConfig>(storageConfig);

// Set up dependencies and create the service worker
builder.Services.AddSingleton<IDatabaseManager, MultiFileDatabaseManager>();
builder.Services.AddSingleton(_ => {
    CompressionLevel compressionLevel = config.GetValue<CompressionLevel>(nameof(ArchiverConfig.CompressionLevel));
    return new ArchiveSizeHelper(compressionLevel);
});
builder.Services.AddSingleton<IArchiver, Archiver.Archiver>();
builder.Services.AddSingleton<IStorageClient, B2Client>(_ => {
    IDictionary envVars = Environment.GetEnvironmentVariables();

    string encryptedKey = storageConfig.GetValue<string>(nameof(StorageConfig.ApplicationKeyEnc));

    if (envVars.Contains("ENC_KEY") == false || envVars.Contains("ENC_IV") == false)
    {
        throw new Exception("The key and the init vector are not both present in the environment");
    }

    string key = (string)envVars["ENC_KEY"];
    string iv = (string)envVars["ENC_IV"];

    string appKey = EncryptorUtil.DecryptString(encryptedKey, Encoding.Default.GetBytes(key), Encoding.Default.GetBytes(iv));

    return new B2Client(storageConfig.GetValue<string>(nameof(StorageConfig.AccountId)), appKey);
});
builder.Services.AddHostedService<StorageCleanupServiceWorker>();
builder.Services.AddHostedService<ArchiverServiceWorker>();

// Instantiate and run our service
IHost host = builder.Build();
host.Run();
