using System.Collections;
using System.Collections.Concurrent;
using Archiver;
using Archiver.Config;
using Archiver.Exceptions;
using Archiver.Extensions;
using Archiver.Utilities;
using Microsoft.Extensions.Options;

namespace ArchiverService;

public class ArchiverServiceWorker(IArchiver archiver, ILogger<ArchiverServiceWorker> logger, IOptions<ArchiverConfig> config) : BackgroundService
{
    private readonly IArchiver _archiver = archiver;
    private readonly ILogger<ArchiverServiceWorker> _logger = logger;
    private readonly ArchiverConfig _config = config.Value;
    private readonly ConcurrentDictionary<string, PollingFileSystemMonitor> _fileSystemWatchers = new();

    private readonly ConcurrentDictionary<string, PollingFileSystemEventArgsCollection> _workQueueHash = [];

    private readonly ConcurrentQueue<FileSystemWatcherQueueEvent> _workQueue = new();

    public override async Task StartAsync(CancellationToken cancellationToken)
    {
        string[] rootPaths = GetRootPathsFromEnvironmentOrConfig();
 
        bool initialized = await _archiver.Init(cancellationToken);

        if (initialized == false)
        {
            _logger.LogError("The archiver did not initialize successfully!");
            throw new ArchiverInitializationException();
        }

        foreach (string rootPath in rootPaths)
        {
            if (Path.Exists(rootPath) == false)
            {
                _logger.LogError("The root path does not exist on the system: {rootPath}", rootPath);
            }
            PollingFileSystemMonitor filesWatcher = BuildWatcherForFiles(rootPath);
            _logger.LogInformation("Built a files watcher for root path {rootPath}. Adding it to the collection of watchers.", rootPath);
            _fileSystemWatchers.TryAdd(rootPath, filesWatcher);
        }

        await base.StartAsync(cancellationToken);

        PollingFileSystemMonitor[] monitors = [.. _fileSystemWatchers.Values];

        await Task.Run(() => monitors.Select(m => m.StartAsync(cancellationToken)).ToArray(), cancellationToken);

        _logger.LogInformation("Finished starting the application");
    }

    private string[] GetRootPathsFromEnvironmentOrConfig()
    {
        HashSet<string> rootVarKeys = [];
        IDictionary envVars = Environment.GetEnvironmentVariables();

        foreach (DictionaryEntry entry in envVars)
        {
            if (entry.Key.ToString().StartsWith("root", StringComparison.CurrentCultureIgnoreCase))
            {
                rootVarKeys.Add(entry.Key.ToString());
            }
        }

        string[] rootPaths = rootVarKeys.Select(k => envVars[k] as string).ToArray();

        if (rootPaths.Length > 0)
        {
            _config.RootPaths = rootPaths;
            _logger.LogInformation("Root paths were found in the environment. Returning those instead of the paths defined in the application settings.");
            return rootPaths;
        }

        _logger.LogInformation("Root paths were not found in the environment. Returning the paths defined in the application settings.");

        return _config.RootPaths;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Listening for work from the work queue.");
        while (!stoppingToken.IsCancellationRequested)
        {
            if (_logger.IsEnabled(LogLevel.Information))
            {
                _logger.LogDebug("Worker running at: {time}", DateTimeOffset.Now);
            }

            try
            {
                _logger.LogDebug("There are {count} items on the work queue.", _workQueue.Count);
                while (_workQueue.Count > 0)
                {
                    if (_workQueue.TryDequeue(out FileSystemWatcherQueueEvent e))
                    {
                        _logger.LogDebug("Pulling event out of work queue and processing it.");
                        await ProcessWorkQueueEvent(e, stoppingToken);
                    }
                    else
                    {
                        _logger.LogError("Could not retrieve the next item from the queue.");
                    }
                }
            }
            catch (System.Exception e)
            {
                _logger.LogCritical("There was a problem tracking changes to the archives: {message}", e.Message);
                throw;
            }
            await Task.Delay(1000, stoppingToken);
        }
    }

    private async Task ProcessWorkQueueEvent(FileSystemWatcherQueueEvent e, CancellationToken token)
    {
        try
        {
            switch (e.Event.ChangeType)
            {
                case PollingFileSystemEventType.Created:
                    await _archiver.OnCreate(e.Event, token);
                    break;
                case PollingFileSystemEventType.Deleted:
                    await _archiver.OnDelete(e.Event, token);
                    break;
                case PollingFileSystemEventType.Changed:
                    await _archiver.OnChange(e.Event, token);
                    break;
                default:
                    throw new NotSupportedException($"The change type: {e.Event} is not supported.");
            }
        }
        catch (Exception ex) when (ex is not IgnorableFileTypeException)
        {
            _logger.LogError(ex, "Failed to process event.");
        }
        catch (IgnorableFileTypeException)
        {
            _logger.LogInformation("Ignoring the file event: {event}", e.Event.Name);
        }

        _workQueueHash.Remove(e.Event.ToHashKey(), out _);
        await Task.Delay(1, token);
    }

    private PollingFileSystemMonitor BuildWatcherForFiles(string rootPath)
    {
        _logger.LogInformation("Building a file watcher for the path: {rootPath}", rootPath);
        PollingFileSystemMonitor watcher = new(rootPath, _logger, 5000, 60000);
        watcher.Created += (_, e) => EnqueueEvent(e);
        watcher.Deleted += (_, e) => EnqueueEvent(e);
        watcher.Changed += (_, e) => EnqueueEvent(e);

        return watcher;
    }

    private void EnqueueEvent(PollingFileSystemEventArgsCollection e)
    {
        using (SyncDispatcher.Enter(this))
        {
            foreach (string fileDetected in e.Files)
            {
                PollingFileSystemEventArgs fileEvent = new(e.ChangeType, fileDetected, Path.GetRelativePath(e.RootPath, fileDetected));
                _logger.LogInformation("Received a new event of type {changeType} with name: {name}", e.ChangeType, fileDetected);
                string hashKey = fileEvent.ToHashKey();
                if (_workQueueHash.ContainsKey(hashKey) == false)
                {
                    _workQueueHash.TryAdd(hashKey, e);
                    _logger.LogInformation("Putting the event on the work queue.");
                    _workQueue.Enqueue(new(fileEvent));
                }
            }
        }
    }
}
