using Archiver.Clients;
using Archiver.Config;
using Microsoft.Extensions.Options;

namespace ArchiverService
{
    public class StorageCleanupServiceWorker(ILogger<StorageCleanupServiceWorker> logger, IOptions<StorageConfig> config, IStorageClient storageClient) : BackgroundService
    {
        private readonly ILogger<StorageCleanupServiceWorker> _logger = logger;
        private readonly IStorageClient _storageClient = storageClient;

        private const int _delayTimeMillis = 1000 * 60 * 60 * 24;

        private readonly Dictionary<string, string> _fileNameTimestampToFileIdIndex = [];

        private readonly Dictionary<string, SortedSet<long>> _fileNameToVersionTimestampsIndex = [];

        private readonly StorageConfig _storageConfig = config.Value;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Running the clean up job for archive storage");
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogDebug("Worker running at: {time}", DateTimeOffset.Now);
                }

                await RunCleanup(stoppingToken);

                _logger.LogInformation("Clean-up done; sleeping for 24 hours. Next run at {nextTime} UTC", DateTime.UtcNow.AddMilliseconds(_delayTimeMillis).ToString("F"));
                await Task.Delay(_delayTimeMillis, stoppingToken);
            }
        }

        private async Task RunCleanup(CancellationToken stoppingToken, string nextFileId = null, string nextFileName = null)
        {
            B2Client.B2ListFileVersionsResponse response = await _storageClient.ListFileVersionsAsync(_storageConfig.BucketId, startFileId: nextFileId, startFileName: nextFileName, token: stoppingToken);

            foreach (B2Client.FileVersion fileVersion in response.Files)
            {
                if (_fileNameToVersionTimestampsIndex.TryAdd(fileVersion.FileName, [fileVersion.UploadTimestamp]) == false)
                {
                    _fileNameToVersionTimestampsIndex[fileVersion.FileName].Add(fileVersion.UploadTimestamp);
                }

                _fileNameTimestampToFileIdIndex.Add($"{fileVersion.FileName}::{fileVersion.UploadTimestamp}", fileVersion.FileId);
            }

            foreach (KeyValuePair<string, SortedSet<long>> item in _fileNameToVersionTimestampsIndex.Where(kvp => kvp.Value.Count > 1))
            {
                SortedSet<long> set = item.Value;
                while (set.Count > 1)
                {
                    long timestamp = set.Min;

                    string indexKey = $"{item.Key}::{timestamp}";

                    string fileIdOfFileToDelete = _fileNameTimestampToFileIdIndex[indexKey];

                    _logger.LogInformation("Removing an old version of file, {fileName}: {fileId} with timestamp {timestamp}", item.Key, fileIdOfFileToDelete, timestamp);

                    await _storageClient.DeleteFileVersionAsync(fileIdOfFileToDelete, item.Key, stoppingToken);

                    _fileNameTimestampToFileIdIndex.Remove(indexKey);

                    set.Remove(timestamp);
                }
            }

            if (string.IsNullOrWhiteSpace(response.NextFileId))
            {
                return;
            }

            await RunCleanup(stoppingToken, response.NextFileId, response.NextFileName);
        }
    }
}