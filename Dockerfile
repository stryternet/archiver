# Use the official .NET SDK image to build the application
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the .csproj file and restore any dependencies
COPY ArchiverService/ ./ArchiverService
COPY Archiver/ ./Archiver

WORKDIR /app/ArchiverService
RUN dotnet restore
RUN dotnet publish -c Release -o out

######################################
####### Run the application ##########
######################################

# Use the official .NET runtime image to run the application
FROM mcr.microsoft.com/dotnet/runtime:8.0

# Set the working directory inside the container
WORKDIR /app

# Copy the build output from the build stage
COPY --from=build /app/ArchiverService/out .

# Define the entry point for the application
ENTRYPOINT ["dotnet", "ArchiverService.dll"]
