using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using static Archiver.Clients.B2Client;

namespace Archiver.Clients
{
    public interface IStorageClient
    {
        Task DeleteFileVersionAsync(string fileId, string fileName, CancellationToken token);
        Task<B2ListFileVersionsResponse> ListFileVersionsAsync(string bucketId, CancellationToken token, string startFileName = null, string startFileId = null, int maxFileCount = 100);
    }

    public class B2Client : IStorageClient
    {
        private readonly string _accountId;
        private readonly string _applicationKey;
        private string _apiUrl;
        private string _authToken;
        private DateTime _tokenAcquiredTime;

        private JsonSerializerOptions _jsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };

        public B2Client(string accountId, string applicationKey)
        {
            _accountId = accountId;
            _applicationKey = applicationKey;
        }

        private async Task AuthorizeAccountAsync(CancellationToken token)
        {
            using HttpClient httpClient = new HttpClient();
            string authHeaderValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{_accountId}:{_applicationKey}"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://api.backblazeb2.com/b2api/v2/b2_authorize_account");
            request.Headers.TryAddWithoutValidation("Authorization", $"Basic {authHeaderValue}");

            HttpResponseMessage response = await httpClient.SendAsync(request, token);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync(token);
                B2AuthorizeAccountResponse result = JsonSerializer.Deserialize<B2AuthorizeAccountResponse>(responseBody, _jsonSerializerOptions);

                _apiUrl = result.ApiUrl;
                _authToken = result.AuthorizationToken;
                _tokenAcquiredTime = DateTime.UtcNow;
            }
            else
            {
                string errorResponse = await response.Content.ReadAsStringAsync(token);
                throw new Exception($"Error authorizing account: {errorResponse}");
            }
        }

        private async Task EnsureAuthorizationAsync(CancellationToken token)
        {
            // Check if 20 hours have passed since the token was acquired
            if (string.IsNullOrEmpty(_authToken) || DateTime.UtcNow - _tokenAcquiredTime > TimeSpan.FromHours(20))
            {
                await AuthorizeAccountAsync(token);
            }
        }

        public async Task DeleteFileVersionAsync(string fileId, string fileName, CancellationToken token)
        {
            await EnsureAuthorizationAsync(token);

            using HttpClient httpClient = new HttpClient();
            string requestUrl = $"{_apiUrl}/b2api/v2/b2_delete_file_version";
            StringContent content = new StringContent(JsonSerializer.Serialize(new
            {
                fileId,
                fileName
            }), System.Text.Encoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, requestUrl);
            request.Headers.TryAddWithoutValidation("Authorization", _authToken);
            request.Content = content;

            HttpResponseMessage response = await httpClient.SendAsync(request, token);

            if (!response.IsSuccessStatusCode)
            {
                string errorResponse = await response.Content.ReadAsStringAsync(token);
                throw new Exception($"Error deleting file version: {errorResponse}");
            }
        }

        public async Task<B2ListFileVersionsResponse> ListFileVersionsAsync(string bucketId, CancellationToken token, string startFileName = null, string startFileId = null, int maxFileCount = 100)
        {
            await EnsureAuthorizationAsync(token);

            using HttpClient httpClient = new HttpClient();
            string requestUrl = $"{_apiUrl}/b2api/v2/b2_list_file_versions";
            StringContent content = new StringContent(JsonSerializer.Serialize(new
            {
                bucketId,
                startFileName,
                startFileId,
                maxFileCount
            }), System.Text.Encoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, requestUrl);
            request.Headers.TryAddWithoutValidation("Authorization", _authToken);
            request.Content = content;

            HttpResponseMessage response = await httpClient.SendAsync(request, token);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync(token);
                B2ListFileVersionsResponse listResponse = JsonSerializer.Deserialize<B2ListFileVersionsResponse>(responseBody, _jsonSerializerOptions);
                return listResponse;
            }
            else
            {
                string errorResponse = await response.Content.ReadAsStringAsync(token);
                throw new Exception($"Error listing file versions: {errorResponse}");
            }
        }

        private class B2AuthorizeAccountResponse
        {
            public string AccountId { get; set; }
            public string ApiUrl { get; set; }  // Moved from StorageApi to here
            public ApiInfo ApiInfo { get; set; }
            public string AuthorizationToken { get; set; }
            public long? ApplicationKeyExpirationTimestamp { get; set; }
        }

        private class ApiInfo
        {
            public StorageApi StorageApi { get; set; }
            public GroupsApi GroupsApi { get; set; }
        }

        private class StorageApi
        {
            public long AbsoluteMinimumPartSize { get; set; }
            public string DownloadUrl { get; set; }
            public string S3ApiUrl { get; set; }
            public List<string> Capabilities { get; set; }
            public string InfoType { get; set; }
            public long RecommendedPartSize { get; set; }
            public string BucketId { get; set; }
            public string BucketName { get; set; }
            public string NamePrefix { get; set; }
        }

        private class GroupsApi
        {
            public string GroupsApiUrl { get; set; }
            public List<string> Capabilities { get; set; }
            public string InfoType { get; set; }
        }

        public class B2ListFileVersionsResponse
        {
            public List<FileVersion> Files { get; set; }
            public string NextFileName { get; set; }
            public string NextFileId { get; set; }
        }

        public class FileVersion
        {
            public string FileId { get; set; }
            public string FileName { get; set; }
            public string Action { get; set; }
            public string ContentType { get; set; }
            public long ContentLength { get; set; }
            public long UploadTimestamp { get; set; }
        }
    }
}
