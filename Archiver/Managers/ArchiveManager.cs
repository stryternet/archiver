using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Exceptions;
using Archiver.Helpers;
using Archiver.Managers;
using Archiver.Managers.DatabaseManager;
using Archiver.Utilities;
using Microsoft.Extensions.Logging;

namespace Archiver
{
    public class ArchiveManager
    {
        private readonly string _directoryToArchive;
        private readonly string _archiveStoragePath;

        private readonly string _archiveName;

        private readonly string _archiveNameOfParentFileWithExtension;

        private readonly long _sizeLimitBytes;
        private readonly IDatabaseManager _databaseManager;
        private readonly ArchiveSizeHelper _archiveSizeHelper;
        private readonly Dictionary<string, ArchiveFile> _zipArchivesIndex = [];
        private readonly SortedDictionary<long, SortedSet<string>> _archiveFilesByRemainingSpaceInBytes = [];

        private ArchiveFile _currentZipArchive;

        private int _maxZipArchivePartCounter = 1;

        private CompressionLevel _compressionLevel;
        private readonly Microsoft.Extensions.Logging.ILogger<Archiver> _logger;

        #region Events

        public event AddFilesEvent AddFiles;

        public event FilesAddedEvent FilesAdded;

        public event FileAddedFailureEvent FilesAddedFailure;

        #endregion

        public ArchiveManager(string directoryToArchive, string archiveStoragePath, int sizeLimitMB, CompressionLevel compressionLevel, IDatabaseManager databaseManager, ArchiveSizeHelper archiveSizeHelper, ILogger<Archiver> logger)
        {
            _logger = logger;
            if (Directory.Exists(directoryToArchive) == false)
            {
                throw new ArgumentException($"is a path that does not exist {directoryToArchive}", nameof(directoryToArchive));
            }

            if (Directory.Exists(archiveStoragePath) == false)
            {
                throw new ArgumentException($"is a path that does not exist {archiveStoragePath}", nameof(archiveStoragePath));
            }

            _directoryToArchive = directoryToArchive;
            _archiveStoragePath = archiveStoragePath;

            _archiveName = ArchiveUtility.BuildArchiveNameFromDirectoryToArchive(_directoryToArchive)!;
            _archiveNameOfParentFileWithExtension = $"{_archiveName}.001.zip";

            _sizeLimitBytes = sizeLimitMB * 1024L * 1024L; // Convert the size in MB to bytes
            _compressionLevel = compressionLevel;
            _databaseManager = databaseManager;
            _archiveSizeHelper = archiveSizeHelper;
        }

        public async Task Init(CancellationToken token)
        {
            (bool Exists, HashSet<string> MissingArchives) = await ArchivesExist(token);
            if (Exists == false)
            {
                _logger.LogInformation("Archives do not exist for path ({}); rebuilding archives.", _directoryToArchive);
                await RebuildArchives(MissingArchives, token);
            }
            
            await LoadArchives(MissingArchives, token);

            await SetActiveArchive(token);
        }

        public async Task AddFilesToArchive(ImmutableHashSet<string> filesToAdd, CancellationToken token)
        {
            AddFiles?.Invoke(filesToAdd.Count);
            int modulus = filesToAdd.Count > 1000 ? 25 : 1;

            int progress = 0;
            Dictionary<string, Exception> filesFailedToBeAdded = [];
            foreach (string fileToAdd in filesToAdd)
            {
                if (token.IsCancellationRequested)
                {
                    break;
                }
                progress++;
                try
                {
                    await AddOrUpdateFileInArchive(fileToAdd, token);
                }
                catch (Exception ex)
                {
                    filesFailedToBeAdded.Add(fileToAdd, ex);
                }

                if (progress % modulus == 0)
                {
                    FilesAdded?.Invoke(progress);
                }
            }

            if (filesFailedToBeAdded.Count > 0)
            {
                FilesAddedFailure?.Invoke(filesFailedToBeAdded);
            }
        }

        public async Task AddOrUpdateFileInArchive(string fileToUpdate, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(fileToUpdate))
                throw new ArgumentException("must be provided", nameof(fileToUpdate));

            if (token.IsCancellationRequested)
                return;

            (ArchiveFile afToUpdate, bool fileExistsInArchiveFile) = await GetArchiveFileForFileNameOrCurrentArchiveFile(fileToUpdate, token);

            // At this point, we're going all in. No cancellation until it is done.
            token = CancellationToken.None;

            if (ArchiveFileHasRoom(afToUpdate, fileToUpdate, out long anticipatedSizeInBytes) == false)
            {
                if (fileExistsInArchiveFile)
                {
                    // We're doing an update on a file and the updated file will no longer fit in the archive. Remove it from its
                    // current archive file so that we can add it to the new one.
                    await RemoveFileFromArchive(fileToUpdate, token);
                }
                afToUpdate = await GetNextAvailableZipArchiveForAnticipatedSize(anticipatedSizeInBytes, token);
            }

            RemoveArchiveFromSizeRegistry(afToUpdate);

            try
            {
                afToUpdate.AddOrUpdateEntry(fileToUpdate);
            }
            catch (System.Exception e)
            {
                e.ToString();
                throw;
            }

            await _databaseManager.AddFileToArchive(fileToUpdate, afToUpdate.Name, token);

            await AddArchiveToSizeRegistry(afToUpdate, token);
        }

        public async Task RemoveFileFromArchive(string fileToRemove, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(fileToRemove))
                throw new ArgumentException("must be provided", nameof(fileToRemove));

            (ArchiveFile afToUpdate, bool _) = await GetArchiveFileForFileNameOrCurrentArchiveFile(fileToRemove, token);

            // At this point, we're going all in. No cancellation until it is done.
            token = CancellationToken.None;

            RemoveArchiveFromSizeRegistry(afToUpdate);

            afToUpdate.DeleteEntry(fileToRemove);

            await _databaseManager.RemoveFileFromArchiveRecord(fileToRemove, afToUpdate.Name, token);

            await AddArchiveToSizeRegistry(afToUpdate, token);
        }

        public async Task<bool> IsTrackingFile(string filePath, CancellationToken token)
        {
            return await _databaseManager.HasFile(filePath, token);
        }

        public async Task RebuildDatabaseFileFromArchiveContents(CancellationToken token)
        {
            foreach (ArchiveFile archive in _zipArchivesIndex.Values)
            {
                foreach (string entry in archive.GetEntries())
                {
                    if (await _databaseManager.HasFile(entry, token) == false)
                    {
                        await _databaseManager.AddFileToArchive(entry, archive.Name, token);
                    }
                }
            } 
        }  

        private async Task SetActiveArchive(CancellationToken token)
        {
            _logger.LogInformation("Getting the name of the next active archive file...");
            string nameOfActiveArchive = await _databaseManager.GetActiveArchiveNameForRoot(_directoryToArchive, token);
            _logger.LogInformation("The next active archive file under the directory ({dir}) is: {name}", _directoryToArchive, nameOfActiveArchive);

            ArchiveFile af = _zipArchivesIndex[nameOfActiveArchive];

            _currentZipArchive = af;
        }

        private async Task RebuildArchives(HashSet<string> archivesToBuild, CancellationToken token)
        {
            if (archivesToBuild == null || archivesToBuild.Count == 0)
            {
                ArchiveFile af = await RegisterNewArchiveFile(_archiveNameOfParentFileWithExtension, token);
                _currentZipArchive = af;

                return;
            }

            foreach (string archiveToBuild in archivesToBuild)
            {
                await RebuildArchive(archiveToBuild, token);
            }
        }

        private async Task RebuildArchive(string archiveToBuild, CancellationToken token)
        {
            _currentZipArchive = await RegisterNewArchiveFile(archiveToBuild, token);

            ImmutableHashSet<string> files = await _databaseManager.GetFilesForArchiveName(archiveToBuild, token);

            foreach (string file in files)
            {
                await AddOrUpdateFileInArchive(_directoryToArchive + file, token);
            }
        }

        private async Task LoadArchives(HashSet<string> rebuiltArchivesToExclude, CancellationToken token)
        {
            int counter = 1;
            string archivePartFileName = $"{_archiveName}.{counter:000}.zip";
            string currentArchivePartPath = Path.Combine(_archiveStoragePath, archivePartFileName);
            while (Path.Exists(currentArchivePartPath) && _zipArchivesIndex.ContainsKey(archivePartFileName) == false)
            {
                if (rebuiltArchivesToExclude.Contains(archivePartFileName) == false)
                {
                    _logger.LogInformation("Registering archive file: {name}", archivePartFileName);
                    ArchiveFile af = await RegisterNewArchiveFile(archivePartFileName, token);
                    long bytesLeft = _sizeLimitBytes - af.CompressedSizeInBytes;
                    if (_archiveFilesByRemainingSpaceInBytes.TryAdd(bytesLeft, [af.Name]) == false)
                    {
                        _archiveFilesByRemainingSpaceInBytes[bytesLeft].Add(af.Name);
                    }
                    _currentZipArchive = af;
                }
                archivePartFileName = $"{_archiveName}.{++counter:000}.zip";
                currentArchivePartPath = Path.Combine(_archiveStoragePath, $"{_archiveName}.{counter:000}.zip");
            }

            _maxZipArchivePartCounter = counter - 1;
        }

        private async Task<(bool Exists, HashSet<string> MissingArchives)> ArchivesExist(CancellationToken token)
        {
            HashSet<string> missingArchives = [];
            _logger.LogInformation("Checking to see if root ({root}) is contained in database", _directoryToArchive);
            if (await _databaseManager.HasRoot(_directoryToArchive, token) == false)
            {
                _logger.LogInformation("The root ({root}) is not contained in database. Returning false", _directoryToArchive);
                return (false, missingArchives);
            }
            _logger.LogInformation("The root ({root}) is contained in database. Getting archive names for root.", _directoryToArchive);
            ImmutableHashSet<string> archiveNames = await _databaseManager.GetArchiveNamesForRoot(_directoryToArchive, token);

            if (archiveNames == null || archiveNames.Count == 0)
            {
                _logger.LogInformation("There are no known archives for root ({root})", _directoryToArchive);
                return (false, missingArchives);
            }

            foreach (string archiveName in archiveNames)
            {
                _logger.LogInformation("Found archive ({name}) for root ({root}). Checking to see if it exists on disk", archiveName, _directoryToArchive);
                bool archiveExists = Path.Exists(Path.Combine(_archiveStoragePath, archiveName));

                if (archiveExists == false)
                {
                    _logger.LogInformation("The archive ({name}) for root ({root}) DOES NOT exist on disk", archiveName, _directoryToArchive);
                    missingArchives.Add(archiveName);
                }
            } 

            return (missingArchives.Count == 0, missingArchives);
        }

        private async Task<(ArchiveFile ArchiveFile, bool FileExistsInArchiveFile)> GetArchiveFileForFileNameOrCurrentArchiveFile(string fileName, CancellationToken token)
        {
            bool fileExistsInArchiveFile = false;
            string archiveFileName = await _databaseManager.GetArchiveFileNameByFileEntryName(fileName, token);

            if (string.IsNullOrWhiteSpace(archiveFileName) == false && _zipArchivesIndex.TryGetValue(archiveFileName, out ArchiveFile archiveFile))
            {
                fileExistsInArchiveFile = true;
                return (archiveFile, fileExistsInArchiveFile);
            }

            return (GetCurrentZipArchive(), fileExistsInArchiveFile);
        }

        private async Task<ArchiveFile> GetNextAvailableZipArchiveForAnticipatedSize(long anticipatedSizeInBytes, CancellationToken token)
        {
            ArchiveFile nextArchiveFileToUse = null;
            HashSet<long> toPurge = [];
            SortedSet<long> sizes = new(_archiveFilesByRemainingSpaceInBytes.Keys);
            foreach (long remainingSpaceInBytes in sizes)
            {
                SortedSet<string> set = _archiveFilesByRemainingSpaceInBytes[remainingSpaceInBytes];
                if (set == null || set.Count == 0)
                {
                    toPurge.Add(remainingSpaceInBytes);
                    continue;
                }
                if (anticipatedSizeInBytes < remainingSpaceInBytes)
                {
                    string archiveFileName = set.FirstOrDefault();

                    nextArchiveFileToUse = _zipArchivesIndex[archiveFileName];

                    break;
                }
            }

            foreach (long item in toPurge)
            {
                _archiveFilesByRemainingSpaceInBytes.Remove(item);
            }

            if (nextArchiveFileToUse == null)
            {
                string archivePartName = $"{_archiveName}.{++_maxZipArchivePartCounter:000}.zip";
                nextArchiveFileToUse = await RegisterNewArchiveFile(archivePartName, token);
            }

            _currentZipArchive = nextArchiveFileToUse;

            return _currentZipArchive;
        }

        private bool ArchiveFileHasRoom(ArchiveFile afToUpdate, string fileToInclude, out long anticipatedSizeInBytes)
        {
            long compressedSizeInBytesOfArchive = afToUpdate.CompressedSizeInBytes;

            _archiveSizeHelper.AddFile(fileToInclude);
            anticipatedSizeInBytes = _archiveSizeHelper.GetCompressedSize(fileToInclude);

            // If the size of the file to be zipped is greater than the size limit of the archive and the archive is currently empty
            // (so this would be the first file added) then go ahead and add the file to the archive. It will exceed the limit, but
            // it will be the only item stored in the zip. Subsequent files will be added to a new archive file.
            if (anticipatedSizeInBytes > _sizeLimitBytes && (compressedSizeInBytesOfArchive == 0 || afToUpdate.HasEntries() == false))
            {
                return true;
            }

            return compressedSizeInBytesOfArchive + anticipatedSizeInBytes < _sizeLimitBytes;
        }

        private async Task AddArchiveToSizeRegistry(ArchiveFile zipArchive, CancellationToken token)
        {
            long bytesRemaining = _sizeLimitBytes - zipArchive.CompressedSizeInBytes;
            if (_archiveFilesByRemainingSpaceInBytes.TryAdd(bytesRemaining, [zipArchive.Name]) == false)
            {
                _archiveFilesByRemainingSpaceInBytes[bytesRemaining].Add(zipArchive.Name);
            }

            await _databaseManager.UpdateSizeForArchive(zipArchive.CompressedSizeInBytes, zipArchive.Name, token);
        }

        private void RemoveArchiveFromSizeRegistry(ArchiveFile zipArchive)
        {
            long bytesRemaining = _sizeLimitBytes - zipArchive.CompressedSizeInBytes;
            if (_archiveFilesByRemainingSpaceInBytes.TryGetValue(bytesRemaining, out SortedSet<string> fileNames))
            {
                fileNames.Remove(zipArchive.Name);

                if (fileNames.Count == 0)
                {
                    _archiveFilesByRemainingSpaceInBytes.Remove(bytesRemaining);
                }
            }
        }

        private ArchiveFile GetCurrentZipArchive()
        {
            return _currentZipArchive;
        }

        private async Task<ArchiveFile> RegisterNewArchiveFile(string archiveFileName, CancellationToken token)
        {
            string archiveFilePathName = Path.Combine(_archiveStoragePath, archiveFileName);
            ArchiveFile af = new(archiveFileName, archiveFilePathName, _compressionLevel);
            _zipArchivesIndex.TryAdd(archiveFileName, af);

            await _databaseManager.RegisterNewArchiveFile(archiveFileName, _directoryToArchive, token);

            return af;
        }
    }
}