using System;
using System.Collections.Generic;

namespace Archiver.Managers;

public delegate void AddFilesEvent(int numberOfFilesToAdd);

public delegate void FilesAddedEvent(int numberOfFilesAddedSinceLastEvent);

public delegate void FileAddedFailureEvent(Dictionary<string, Exception> failedFiles);