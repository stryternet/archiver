namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager
{
    static class MultiFileDatabaseHeaders
    {
        public const string RootPathsHeader = "[RootPaths]";
        public const string FilesHeader = "[Files]";
        public const string FileHeader = "[File(";

        public const string SpaceRemainingInBytesRegex = @"SpaceRemainingInBytes=(-?\d+)";
    }
}