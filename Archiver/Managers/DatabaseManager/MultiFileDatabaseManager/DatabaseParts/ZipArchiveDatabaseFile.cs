using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Exceptions;

namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts
{
    public partial class ZipArchiveDatabaseFile : DatabaseFile<string>
    {

        public override string PathToFile { get; init; }

        public override string Name { get; init; }

        public string ArchiveFileName { get; init; }

        public HashSet<string> Files { get; init; } = [];

        public ConcurrentDictionary<string, DateTime> FilesWithLastModDate { get; init; } = [];

        public long SpaceRemainingInBytes { get; private set; }

        [GeneratedRegex(MultiFileDatabaseHeaders.SpaceRemainingInBytesRegex)]
        private static partial Regex SpaceRemainingInBytesRegex();

        private ZipArchiveDatabaseFile(string pathToFile, string name, long spaceRemainingInBytes)
        {
            PathToFile = pathToFile;
            Name = name;
            ArchiveFileName = name.Replace(".adb", "");
            SpaceRemainingInBytes = spaceRemainingInBytes;
        }

        public Task Add(string pathToFile, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                return Task.CompletedTask;
            }
            FilesWithLastModDate.AddOrUpdate(pathToFile, File.GetLastWriteTimeUtc(pathToFile), (k,v) => {
                return File.GetLastWriteTimeUtc(pathToFile);
            });
            Files.Add(pathToFile);

            return Task.CompletedTask;
        }

        public Task Remove(string pathToFile, CancellationToken token)
        {
            if (token.IsCancellationRequested == false && FilesWithLastModDate.TryRemove(pathToFile, out _))
            {
                Files.Remove(pathToFile);
            }

            return Task.CompletedTask;
        }

        public async Task MoveAllFilesFromOldPathToNewPath(string oldDirectory, string newDirectory, CancellationToken token)
        {
            Dictionary<string, string> updatedFiles = [];
            foreach (string file in Files)
            {
                if (file.StartsWith(oldDirectory) == false)
                {
                    continue;
                }

                string updatedFileName = file.Replace(oldDirectory, newDirectory);

                updatedFiles.Add(file, updatedFileName);
            }

            Files.RemoveWhere(updatedFiles.ContainsKey);

            Files.UnionWith(updatedFiles.Values);

            foreach (var item in updatedFiles)
            {
                FilesWithLastModDate.TryRemove(item.Key, out DateTime _);
                FilesWithLastModDate.TryAdd(item.Value, File.GetLastWriteTimeUtc(item.Value));
            }

            await Persist(token);
        }

        public async Task RenameFile(string oldFile, string newFile, CancellationToken token)
        {
            Files.Add(newFile);
            FilesWithLastModDate.TryAdd(newFile, File.GetLastWriteTimeUtc(newFile));
            Files.Remove(oldFile);
            FilesWithLastModDate.TryRemove(oldFile, out DateTime _);
            await Persist(token);
        }

        public static async Task<ZipArchiveDatabaseFile> FromPathAndName(string databasePath, string archiveDatabaseFileName, CancellationToken token)
        {
            if (File.Exists(Path.Combine(databasePath, archiveDatabaseFileName)) == false)
            {
                throw new InvalidArchiveDbFormatException();
            }
            string[] dbLines = await File.ReadAllLinesAsync(Path.Combine(databasePath, archiveDatabaseFileName), token);

            if (dbLines == null || dbLines.Length == 0 || dbLines[0].StartsWith(MultiFileDatabaseHeaders.FileHeader) == false)
            {
                throw new InvalidArchiveDbFormatException();
            }

            if (dbLines == null || dbLines.Length == 0)
            {
                throw new InvalidArchiveDbFormatException();
            }

            Match match = SpaceRemainingInBytesRegex().Match(dbLines[0]);

            if (match.Success == false || long.TryParse(match.Groups[1].Value, out long spaceRemainingInBytes) == false)
            {
                throw new InvalidArchiveDbFormatException();
            }

            if (dbLines.Length == 1)
            {
                return new ZipArchiveDatabaseFile(databasePath, archiveDatabaseFileName, spaceRemainingInBytes);
            }

            int index = 1; // start with the second line in the file at index 1

            HashSet<string> files = [];
            Dictionary<string, DateTime> filesWithLastModDate = [];
            bool hasLastMod = true;
            do
            {
                if (string.IsNullOrWhiteSpace(dbLines[index]))
                {
                    continue;
                }

                string line = dbLines[index];

                string[] lineParts = line.Split("::");

                string filePart = lineParts[0];

                hasLastMod = lineParts.Length > 1;

                DateTime lastModPart = hasLastMod ? DateTime.Parse(lineParts[1]) : File.GetLastWriteTimeUtc(filePart);

                line = line.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);

                if (line.StartsWith(Path.DirectorySeparatorChar) == false)
                {
                    throw new InvalidArchiveDbFormatException();
                }

                files.Add(filePart);
                filesWithLastModDate.Add(filePart, lastModPart);
            } while (++index < dbLines.Length);

            ZipArchiveDatabaseFile zipFile = new(databasePath, archiveDatabaseFileName, spaceRemainingInBytes)
            {
                Files = files,
                FilesWithLastModDate = new ConcurrentDictionary<string, DateTime>(filesWithLastModDate)
            };

            // If the archive database file did not have a last mod date for all files, then we have done a correction of the
            // database file with the freshest mod dates from disk. We need to persist the archive database file right now
            // to record the last mod date of the files.
            if (hasLastMod == false)
            {
                await zipFile.Persist(token);
            }

            return zipFile;
        }

        public static async Task<ZipArchiveDatabaseFile> CreateFromPathAndName(string pathToFile, string archiveFileName, long maxSizeOfArchiveInBytes, CancellationToken _)
        {
            string fileName = Path.Combine(pathToFile, archiveFileName);
            await File.Create(fileName).DisposeAsync();

            return new ZipArchiveDatabaseFile(pathToFile, archiveFileName, maxSizeOfArchiveInBytes);
        }

        public async Task UpdateRemainingSpaceInBytes(long remainingSpaceInBytes, CancellationToken token)
        {
            SpaceRemainingInBytes = remainingSpaceInBytes;
            await Persist(token);
        }

        protected override async Task<bool> PersistInternal(CancellationToken token)
        {
            HashSet<string> lines = ToTextLines();

            return await PersistLines(lines, token);
        }

        protected override HashSet<string> ToTextLines()
        {
            HashSet<string> lines = [];

            lines.Add($"[File(SpaceRemainingInBytes={SpaceRemainingInBytes})]");

            foreach (string file in Files)
            {
                lines.Add($"{file}::{FilesWithLastModDate[file]:O}");
            }

            return lines;
        }
    }
}