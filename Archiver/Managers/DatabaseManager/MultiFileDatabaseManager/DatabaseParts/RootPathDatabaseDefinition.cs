using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Exceptions;

namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts
{
    public class RootPathDatabaseDefinition
    {
        public string RootPath { get; init; }

        public ConcurrentBag<ZipArchiveDatabaseFile> ArchiveDatabaseFiles { get; init; }

        private readonly ConcurrentDictionary<string, ZipArchiveDatabaseFile> _archiveDatabaseFilesIndex = [];
        private readonly ConcurrentDictionary<long, List<ZipArchiveDatabaseFile>> _sizeRemainingToArchiveDatabaseFilesIndex = [];
        private readonly SortedSet<long> _sizesRemainingLookup = [];

        private RootPathDatabaseDefinition(string rootPath, List<ZipArchiveDatabaseFile> archiveDatabaseFiles)
        {
            RootPath = rootPath;
            ArchiveDatabaseFiles = new ConcurrentBag<ZipArchiveDatabaseFile>(archiveDatabaseFiles);
            BuildIndexes();
        }

        private void BuildIndexes()
        {
            foreach (ZipArchiveDatabaseFile file in ArchiveDatabaseFiles)
            {
                _archiveDatabaseFilesIndex.TryAdd(file.Name, file);
                _archiveDatabaseFilesIndex.TryAdd(file.Name.Replace(".adb", ""), file);
                _sizeRemainingToArchiveDatabaseFilesIndex.AddOrUpdate(file.SpaceRemainingInBytes, [file], (key, value) =>
                {
                    value.Add(file);
                    return value;
                });
                _sizesRemainingLookup.Add(file.SpaceRemainingInBytes);
            }
        }


        public async Task<ZipArchiveDatabaseFile> AddFileToArchive(string nameOfFileToAdd, string archiveFileName, CancellationToken token)
        {
            if (_archiveDatabaseFilesIndex.TryGetValue(archiveFileName, out ZipArchiveDatabaseFile file))
            {
                await file.Add(nameOfFileToAdd, token);

                await file.Persist(token);

                return file;
            }

            throw new ZipArchiveNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveDatabaseFilesIndex.Keys)}");
        }

        public async Task RemoveFileFromArchive(string fileToRemove, string archiveFileName, CancellationToken token)
        {
            if (_archiveDatabaseFilesIndex.TryGetValue(archiveFileName, out ZipArchiveDatabaseFile file))
            {
                await file.Remove(fileToRemove, token);

                await file.Persist(token);

                return;
            }

            throw new ZipArchiveNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveDatabaseFilesIndex.Keys)}");
        }

        public Task<string> GetNameOfNextArchiveFileWithSpaceAvailable(CancellationToken token)
        {
            token.ThrowIfCancellationRequested();

            long nextSmallestSizeRemaining = _sizesRemainingLookup.FirstOrDefault();

            if (_sizeRemainingToArchiveDatabaseFilesIndex.TryGetValue(nextSmallestSizeRemaining, out List<ZipArchiveDatabaseFile> files))
            {
                return Task.FromResult(files[0].Name.Replace(".adb", ""));
            }

            throw new ZipArchiveNotFoundException($"nextSmallestSizeRemaining: {nextSmallestSizeRemaining}");
        }

        public Task<ImmutableHashSet<string>> GetFilesForArchiveName(string archiveName, CancellationToken token)
        {
            if (_archiveDatabaseFilesIndex.TryGetValue(archiveName, out ZipArchiveDatabaseFile file))
            {
                return Task.FromResult<ImmutableHashSet<string>>([.. file.Files]);
            }

            throw new ZipArchiveNotFoundException(archiveName, $"Current archives in index: {string.Join(", ", _archiveDatabaseFilesIndex.Keys)}");
        }

        public async Task MoveAllFilesFromOldPathToNewPathForArchiveFile(string oldDirectory, string newDirectory, string archiveFileName, CancellationToken token)
        {
            if (_archiveDatabaseFilesIndex.TryGetValue(archiveFileName, out ZipArchiveDatabaseFile file))
            {
                await file.MoveAllFilesFromOldPathToNewPath(oldDirectory, newDirectory, token);
            }

            throw new ZipArchiveNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveDatabaseFilesIndex.Keys)}");
        }

        public static async Task<RootPathDatabaseDefinition> FromRootPathAndFiles(string dbFilesPath, string rootPath, HashSet<string> files, CancellationToken token)
        {
            string rootName = Path.GetFileNameWithoutExtension(rootPath);
            List<ZipArchiveDatabaseFile> loadedFiles = await LoadArchiveDatabaseFiles(dbFilesPath, rootName, files, token);
            return new RootPathDatabaseDefinition(rootPath, loadedFiles);
        }

        public async Task RegisterNewArchiveFile(string archiveFileName, string pathToFile, long maxSizeOfArchiveInBytes, CancellationToken token)
        {
            ZipArchiveDatabaseFile newArchive = await ZipArchiveDatabaseFile.CreateFromPathAndName(pathToFile, archiveFileName, maxSizeOfArchiveInBytes, token);

            ArchiveDatabaseFiles.Add(newArchive);

            await newArchive.Persist(token);

            _archiveDatabaseFilesIndex.TryAdd(newArchive.Name, newArchive);
            _archiveDatabaseFilesIndex.TryAdd(newArchive.Name.Replace(".adb", ""), newArchive);
            _sizeRemainingToArchiveDatabaseFilesIndex.AddOrUpdate(newArchive.SpaceRemainingInBytes, [newArchive], (k, v) =>
            {
                v.Add(newArchive);
                return v;
            });
            _sizesRemainingLookup.Add(newArchive.SpaceRemainingInBytes);
        }

        internal async Task UpdateRemainingSpaceInBytesForArchive(long remainingSpaceInBytes, string archiveFileName, CancellationToken token)
        {
            if (_archiveDatabaseFilesIndex.TryGetValue(archiveFileName, out ZipArchiveDatabaseFile file))
            {
                long currentRemainingSpaceInBytes = file.SpaceRemainingInBytes;
                if (_sizeRemainingToArchiveDatabaseFilesIndex.TryGetValue(currentRemainingSpaceInBytes, out List<ZipArchiveDatabaseFile> dbFiles))
                {
                    if (dbFiles.Count > 1)
                    {
                        dbFiles.Remove(file);
                    }
                    else
                    {
                        _sizeRemainingToArchiveDatabaseFilesIndex.TryRemove(currentRemainingSpaceInBytes, out _);
                        _sizesRemainingLookup.Remove(currentRemainingSpaceInBytes);
                    }
                }
                await file.UpdateRemainingSpaceInBytes(remainingSpaceInBytes, token);
                _sizesRemainingLookup.Add(remainingSpaceInBytes);
                _sizeRemainingToArchiveDatabaseFilesIndex.AddOrUpdate(remainingSpaceInBytes, [file], (key, val) =>
                {
                    val.Add(file);
                    return val;
                });

                return;
            }

            throw new ZipArchiveNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveDatabaseFilesIndex.Keys)}");
        }

        private static async Task<List<ZipArchiveDatabaseFile>> LoadArchiveDatabaseFiles(string pathToFiles, string rootFolderName, HashSet<string> files, CancellationToken token)
        {
            List<ZipArchiveDatabaseFile> loadedFiles = [];
            foreach (string file in files)
            {
                if (file.StartsWith(rootFolderName) == false)
                {
                    continue;
                }
                if (file.EndsWith(".adb") == false)
                {
                    throw new InvalidArchiveDbFormatException();
                }

                loadedFiles.Add(await ZipArchiveDatabaseFile.FromPathAndName(pathToFiles, file, token));
            }

            return loadedFiles;
        }

        public static Task<RootPathDatabaseDefinition> CreateFromRootPath(string rootPathToRegister, CancellationToken token)
        {
            return Task.FromResult(new RootPathDatabaseDefinition(rootPathToRegister, []));
        }

    }
}