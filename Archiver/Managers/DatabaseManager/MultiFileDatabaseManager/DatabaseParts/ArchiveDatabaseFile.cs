using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Exceptions;

namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts
{
    public sealed class ArchiveDatabaseFile : DatabaseFile<string>
    {
        public override string Name { get; init; }

        public HashSet<string> RootPaths { get; private set; } = [];
        public override string PathToFile { get; init; }

        public ConcurrentBag<RootPathDatabaseDefinition> RootPathDatabaseDefinitions { get; private set; }

        private readonly ConcurrentDictionary<string, RootPathDatabaseDefinition> _archiveFileNameToRootDatabaseDefinitionIndex = [];
        private readonly ConcurrentDictionary<string, RootPathDatabaseDefinition> _rootPathToRootDatabaseDefinitionIndex = [];
        private readonly ConcurrentDictionary<string, ZipArchiveDatabaseFile> _fileNameToZipArchiveDatabaseFileIndex = [];

        private ArchiveDatabaseFile(string pathToFile, string name, HashSet<string> rootPaths = null, List<RootPathDatabaseDefinition> files = null)
        {
            PathToFile = pathToFile;
            Name = name;
            RootPaths = rootPaths ?? [];
            RootPathDatabaseDefinitions = new ConcurrentBag<RootPathDatabaseDefinition>(files);
            BuildIndexes();
        }

        private void BuildIndexes()
        {
            HashSet<string> allArchivedFiles = [];
            foreach (RootPathDatabaseDefinition definition in RootPathDatabaseDefinitions)
            {
                Dictionary<string, ZipArchiveDatabaseFile> allFileNamesIndex = [];
                foreach (ZipArchiveDatabaseFile archiveDatabaseFile in definition.ArchiveDatabaseFiles)
                {
                    _archiveFileNameToRootDatabaseDefinitionIndex.TryAdd(archiveDatabaseFile.Name, definition);
                    _archiveFileNameToRootDatabaseDefinitionIndex.TryAdd(archiveDatabaseFile.Name.Replace(".adb", ""), definition);

                    Dictionary<string, ZipArchiveDatabaseFile> dict = archiveDatabaseFile.Files.ToDictionary(k => k, _ => archiveDatabaseFile);

                    allFileNamesIndex = allFileNamesIndex.Concat(dict).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                }

                foreach (KeyValuePair<string, ZipArchiveDatabaseFile> item in allFileNamesIndex)
                {
                    _fileNameToZipArchiveDatabaseFileIndex.TryAdd(item.Key, item.Value);
                }

                _rootPathToRootDatabaseDefinitionIndex.TryAdd(definition.RootPath, definition);
            }
        }

        public static async Task<ArchiveDatabaseFile> FromPathAndName(string databasePath, string archiveDatabaseName, CancellationToken token)
        {
            if (File.Exists(Path.Combine(databasePath, archiveDatabaseName)) == false)
            {
                throw new InvalidArchiveDbFormatException();
            }
            string[] dbLines = await File.ReadAllLinesAsync(Path.Combine(databasePath, archiveDatabaseName), token);

            if (dbLines == null || dbLines.Length == 0 || dbLines[0].StartsWith(MultiFileDatabaseHeaders.RootPathsHeader) == false)
            {
                throw new InvalidArchiveDbFormatException();
            }

            if (dbLines == null || dbLines.Length == 0)
            {
                throw new InvalidArchiveDbFormatException();
            }

            if (dbLines.Length == 1)
            {
                return new ArchiveDatabaseFile(databasePath, archiveDatabaseName);
            }

            int index = 1; // start with the second line in the file at index 1

            HashSet<string> rootPaths = [];

            do
            {
                if (string.IsNullOrWhiteSpace(dbLines[index]))
                {
                    continue;
                }

                string line = dbLines[index];

                line = line.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);

                if (line.StartsWith(Path.DirectorySeparatorChar) == false)
                {
                    throw new InvalidArchiveDbFormatException();
                }

                rootPaths.Add(dbLines[index]);

            } while (dbLines[++index] != MultiFileDatabaseHeaders.FilesHeader);

            index++;

            HashSet<string> files = [];

            do
            {
                if (string.IsNullOrWhiteSpace(dbLines[index]))
                {
                    continue;
                }

                if (dbLines[index].EndsWith(".adb") == false)
                {
                    throw new InvalidArchiveDbFormatException();
                }

                files.Add(dbLines[index]);
            } while (++index < dbLines.Length);

            List<RootPathDatabaseDefinition> rootDefinitions = [];

            foreach (string rootPath in rootPaths)
            {
                rootDefinitions.Add(await RootPathDatabaseDefinition.FromRootPathAndFiles(databasePath, rootPath, files, token));
            }

            return new ArchiveDatabaseFile(databasePath, archiveDatabaseName, rootPaths, rootDefinitions);
        }

        public async Task AddFileToArchive(string nameOfFileToAdd, string archiveFileName, CancellationToken token)
        {
            if (_archiveFileNameToRootDatabaseDefinitionIndex.TryGetValue(archiveFileName, out RootPathDatabaseDefinition definition))
            {
                ZipArchiveDatabaseFile archiveAddedTo = await definition.AddFileToArchive(nameOfFileToAdd, archiveFileName, token);

                _fileNameToZipArchiveDatabaseFileIndex.TryAdd(nameOfFileToAdd, archiveAddedTo);
                return;
            }

            throw new ArchiveFileNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveFileNameToRootDatabaseDefinitionIndex.Keys)}");
        }

        public async Task<string> GetActiveArchiveNameForRootPath(string rootDirectory, CancellationToken token)
        {
            if (_rootPathToRootDatabaseDefinitionIndex.TryGetValue(rootDirectory, out RootPathDatabaseDefinition definition))
            {
                return await definition.GetNameOfNextArchiveFileWithSpaceAvailable(token);
            }

            throw new ArchiveFileNotFoundException(rootDirectory, $"Registered root paths: {string.Join(", ", _rootPathToRootDatabaseDefinitionIndex.Keys)}");
        }

        public Task<string> GetArchiveFileNameByFileEntryName(string fileName, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            if (_fileNameToZipArchiveDatabaseFileIndex.ContainsKey(fileName) == false)
            {
                return Task.FromResult(null as string);
            }
            if (_fileNameToZipArchiveDatabaseFileIndex.TryGetValue(fileName, out ZipArchiveDatabaseFile zipArchiveDatabaseFile))
            {
                return Task.FromResult(zipArchiveDatabaseFile.ArchiveFileName);
            }

            throw new ZipArchiveNotFoundException(fileName, $"Current archives in index: {string.Join(", ", _fileNameToZipArchiveDatabaseFileIndex.Keys)}");
        }

        public Task<string[]> GetArchiveFileNamesByDirectoryName(string directoryName, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            if (_rootPathToRootDatabaseDefinitionIndex.TryGetValue(directoryName, out RootPathDatabaseDefinition definition))
            {
                return Task.FromResult(definition.ArchiveDatabaseFiles.Select(f => f.ArchiveFileName).ToArray());
            }

            return Task.FromResult<string[]>([]);
        }

        public async Task<ImmutableHashSet<string>> GetFilesForArchiveName(string archiveName, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            if (_archiveFileNameToRootDatabaseDefinitionIndex.TryGetValue(archiveName, out RootPathDatabaseDefinition definition))
            {
                return await definition.GetFilesForArchiveName(archiveName, token);
            }

            throw new ArchiveFileNotFoundException(archiveName, $"Current archives in index: {string.Join(", ", _archiveFileNameToRootDatabaseDefinitionIndex.Keys)}");
        }

        public async Task MoveAllFilesFromOldPathToNewPathForArchiveFile(string oldDirectory, string newDirectory, string archiveFileName, CancellationToken token)
        {
            if (_archiveFileNameToRootDatabaseDefinitionIndex.TryGetValue(archiveFileName, out RootPathDatabaseDefinition definition))
            {
                await definition.MoveAllFilesFromOldPathToNewPathForArchiveFile(oldDirectory, newDirectory, archiveFileName, token);
                return;
            }

            throw new ArchiveFileNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveFileNameToRootDatabaseDefinitionIndex.Keys)}");
        }

        public async Task RegisterNewArchiveFile(string archiveFileName, string rootDirectory, long maxSizeOfArchiveInBytes, CancellationToken token)
        {
            if (_rootPathToRootDatabaseDefinitionIndex.TryGetValue(rootDirectory, out RootPathDatabaseDefinition definition))
            {
                await definition.RegisterNewArchiveFile(archiveFileName, PathToFile, maxSizeOfArchiveInBytes, token);
                _archiveFileNameToRootDatabaseDefinitionIndex.TryAdd(archiveFileName, definition);
                _archiveFileNameToRootDatabaseDefinitionIndex.TryAdd(archiveFileName.Replace(".adb", ""), definition);

                await Persist(token);
                return;
            }

            throw new ArchiveFileNotFoundException(rootDirectory, $"Registered root paths: {string.Join(", ", _rootPathToRootDatabaseDefinitionIndex.Keys)}");
        }

        public async Task RegisterNewRootPath(string rootPathToRegister, long maxSizeOfArchiveInBytes, CancellationToken token)
        {
            RootPaths.Add(rootPathToRegister);
            string archiveName = Path.GetFileNameWithoutExtension(rootPathToRegister);
            RootPathDatabaseDefinition newRootPathDef = await RootPathDatabaseDefinition.CreateFromRootPath(rootPathToRegister, token);
            RootPathDatabaseDefinitions.Add(newRootPathDef);
            _rootPathToRootDatabaseDefinitionIndex.TryAdd(rootPathToRegister, newRootPathDef);
            
            await RegisterNewArchiveFile($"{archiveName}.001.zip.adb", rootPathToRegister, maxSizeOfArchiveInBytes, token);
        }

        public async Task RemoveFileFromArchiveRecord(string fileToRemove, string archiveFileName, CancellationToken token)
        {
            if (_archiveFileNameToRootDatabaseDefinitionIndex.TryGetValue(archiveFileName, out RootPathDatabaseDefinition definition))
            {
                await definition.RemoveFileFromArchive(fileToRemove, archiveFileName, token);

                _fileNameToZipArchiveDatabaseFileIndex.TryRemove(fileToRemove, out ZipArchiveDatabaseFile _);
                return;
            }

            throw new ArchiveFileNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveFileNameToRootDatabaseDefinitionIndex.Keys)}");
        }

        public async Task RenameFile(string oldFile, string newFile, CancellationToken token)
        {
            if (_fileNameToZipArchiveDatabaseFileIndex.TryGetValue(oldFile, out ZipArchiveDatabaseFile archiveFile))
            {
                await archiveFile.RenameFile(oldFile, newFile, token);
                return;
            }

            throw new ZipArchiveNotFoundException(oldFile, $"Old file: {oldFile}; New file: {newFile}; Current archives in index: {string.Join(", ", _fileNameToZipArchiveDatabaseFileIndex.Keys)}");
        }

        public async Task UpdateSpaceRemainingInBytesForArchive(long spaceRemainingInBytes, string archiveFileName, CancellationToken token)
        {
            if (_archiveFileNameToRootDatabaseDefinitionIndex.TryGetValue(archiveFileName, out RootPathDatabaseDefinition definition))
            {
                await definition.UpdateRemainingSpaceInBytesForArchive(spaceRemainingInBytes, archiveFileName, token);
                return;
            }

            throw new ArchiveFileNotFoundException(archiveFileName, $"Current archives in index: {string.Join(", ", _archiveFileNameToRootDatabaseDefinitionIndex.Keys)}");
        }

        public Task<bool> HasArchiveFile(string archiveFileName, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            bool exists = _archiveFileNameToRootDatabaseDefinitionIndex.ContainsKey(archiveFileName);

            return Task.FromResult(exists);
        }

        public Task<bool> HasFileArchived(string filePath, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();

            bool hasFile = _fileNameToZipArchiveDatabaseFileIndex.ContainsKey(filePath);

            return Task.FromResult(hasFile);
        }

        protected override async Task<bool> PersistInternal(CancellationToken token)
        {
            HashSet<string> lines = ToTextLines();

            return await PersistLines(lines, token);
        }

        protected override HashSet<string> ToTextLines()
        {
            HashSet<string> result = [];

            result.Add("[RootPaths]");

            result.UnionWith(RootPaths);

            result.Add("");

            result.Add("[Files]");

            result.UnionWith(RootPathDatabaseDefinitions.SelectMany(r => r.ArchiveDatabaseFiles).Select(adf => adf.Name).ToList());

            return result;
        }
    }
}