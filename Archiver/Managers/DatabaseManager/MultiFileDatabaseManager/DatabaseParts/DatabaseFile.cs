using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Utilities;

namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts
{
    public abstract class DatabaseFile<TFile>
    {
        public abstract string PathToFile { get; init; 
        }
        public abstract string Name { get; init; }

        public async Task<bool> Persist(CancellationToken token)
        {
            using (SyncDispatcher.Enter(this))
            {
                return await PersistInternal(token);
            }
        }

        protected abstract Task<bool> PersistInternal(CancellationToken token);

        protected abstract HashSet<string> ToTextLines();

        protected async Task<bool> PersistLines(HashSet<string> linesToPersist, CancellationToken token)
        {
            await File.WriteAllLinesAsync(Path.Join(PathToFile, Name), linesToPersist, token);
            return true;
        }
    }
}