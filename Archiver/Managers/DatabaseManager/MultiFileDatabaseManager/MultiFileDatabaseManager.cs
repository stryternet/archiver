using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Config;
using Archiver.Managers.DatabaseManager.MultiFileDatabaseManager.DatabaseParts;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Archiver.Managers.DatabaseManager.MultiFileDatabaseManager;

public class MultiFileDatabaseManager(IOptions<DatabaseConfig> config, IOptions<ArchiverConfig> archiverConfig, ILogger<MultiFileDatabaseManager> logger) : IDatabaseManager
{

    private const string _archiveDatabaseName = "archive.adb";

    private readonly ILogger<MultiFileDatabaseManager> _logger = logger;

    private readonly string _databasePath = config.Value.DatabasePath;

    private readonly long _maxSizeOfArchivesInBytes = Convert.ToInt64(archiverConfig.Value.SizeLimitMB) * 1024 * 1024;

    public bool Initialized { get; private set; }

    public event DatabaseCreated DatabaseCreated;

    private ArchiveDatabaseFile _rootArchiveDatabaseFile;

    public async Task<bool> Init(CancellationToken token)
    {
        _logger.LogInformation("Initializing the Database Manager");
        if (DatabaseExists() == false)
        {
            _logger.LogInformation("The database does not yet exist. Creating a new database.");
            await CreateDatabase(token);
            await LoadDatabaseFiles(token);
            DatabaseCreated?.Invoke();
            return Initialized = true;
        }

        _logger.LogInformation("Loading the database files");

        await LoadDatabaseFiles(token);

        return Initialized = true;
    }

    public async Task AddFileToArchive(string nameOfFileToAdd, string archiveFileName, CancellationToken token)
    {
        await _rootArchiveDatabaseFile.AddFileToArchive(nameOfFileToAdd, ToDatabaseArchiveFileName(archiveFileName), token);
    }

    public async Task<string> GetActiveArchiveNameForRoot(string rootDirectory, CancellationToken token)
    {
        return await _rootArchiveDatabaseFile.GetActiveArchiveNameForRootPath(rootDirectory, token);
    }

    public async Task<string> GetArchiveFileNameByFileEntryName(string fileName, CancellationToken token)
    {
        return await _rootArchiveDatabaseFile.GetArchiveFileNameByFileEntryName(fileName, token);
    }

    public async Task<ImmutableHashSet<string>> GetArchiveNamesForRoot(string rootDirectory, CancellationToken token)
    {
        return (await _rootArchiveDatabaseFile.GetArchiveFileNamesByDirectoryName(rootDirectory, token)).ToImmutableHashSet();
    }

    public async Task<ImmutableHashSet<string>> GetFilesForArchiveName(string archiveName, CancellationToken token)
    {
        return await _rootArchiveDatabaseFile.GetFilesForArchiveName(ToDatabaseArchiveFileName(archiveName), token);
    }

    public Task<ImmutableHashSet<string>> GetRootPaths(CancellationToken token)
    {
        return Task.FromResult(_rootArchiveDatabaseFile.RootPaths.ToImmutableHashSet());
    }

    public Task<bool> HasRoot(string rootPath, CancellationToken token)
    {
        return Task.FromResult(_rootArchiveDatabaseFile.RootPaths.Contains(rootPath));
    }

    public async Task MoveAllFilesFromOldPathToNewPathForArchiveFile(string oldDirectory, string newDirectory, string archiveFileName, CancellationToken token)
    {
        await _rootArchiveDatabaseFile.MoveAllFilesFromOldPathToNewPathForArchiveFile(oldDirectory, newDirectory, ToDatabaseArchiveFileName(archiveFileName), token);
    }

    public async Task RegisterNewArchiveFile(string archiveFileName, string rootDirectory, CancellationToken token)
    {
        bool exists = await _rootArchiveDatabaseFile.HasArchiveFile(archiveFileName, token);
        if (exists == false)
        {
            await _rootArchiveDatabaseFile.RegisterNewArchiveFile(ToDatabaseArchiveFileName(archiveFileName), rootDirectory, _maxSizeOfArchivesInBytes, token);
        }
    }

    public async Task RegisterNewRootPath(string rootPathToRegister, string archiveName, CancellationToken token, bool loadRootSection = false)
    {
        await _rootArchiveDatabaseFile.RegisterNewRootPath(rootPathToRegister, _maxSizeOfArchivesInBytes, token);
    }

    public async Task RemoveFileFromArchiveRecord(string fileToRemove, string archiveFileName, CancellationToken token)
    {
        await _rootArchiveDatabaseFile.RemoveFileFromArchiveRecord(fileToRemove, ToDatabaseArchiveFileName(archiveFileName), token);
    }

    public async Task RenameFile(string oldFile, string newFile, string archiveFileName, CancellationToken token)
    {
        await _rootArchiveDatabaseFile.RenameFile(oldFile, newFile, token);
    }

    public async Task UpdateSizeForArchive(long compressedSizeInBytes, string archiveFileName, CancellationToken token)
    {
        long spaceRemainingInBytes = _maxSizeOfArchivesInBytes - compressedSizeInBytes;
        await _rootArchiveDatabaseFile.UpdateSpaceRemainingInBytesForArchive(spaceRemainingInBytes, ToDatabaseArchiveFileName(archiveFileName), token);
    }

    public async Task<bool> HasFile(string filePath, CancellationToken token)
    {
        return await _rootArchiveDatabaseFile.HasFileArchived(filePath, token);
    }

    private bool DatabaseExists()
    {
        string dbPath = Path.Combine(_databasePath, _archiveDatabaseName);
        bool fileExists = File.Exists(dbPath);

        return fileExists;
    }

    private async Task CreateDatabase(CancellationToken token)
    {
        string dbPath = Path.Combine(_databasePath, _archiveDatabaseName);

        using StreamWriter writer = File.CreateText(dbPath);

        await writer.WriteLineAsync(
            new StringBuilder(MultiFileDatabaseHeaders.RootPathsHeader)
            .AppendLine()
            .AppendLine()
            .AppendLine(MultiFileDatabaseHeaders.FilesHeader), token);
    }

    private async Task LoadDatabaseFiles(CancellationToken token)
    {
        _rootArchiveDatabaseFile = await ArchiveDatabaseFile.FromPathAndName(_databasePath, _archiveDatabaseName, token);
    }

    private static string ToDatabaseArchiveFileName(string archiveFileName)
    {
        return archiveFileName.EndsWith("adb") ? archiveFileName : $"{archiveFileName}.adb";
    }
}