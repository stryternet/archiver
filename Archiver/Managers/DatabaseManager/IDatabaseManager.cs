using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

namespace Archiver.Managers.DatabaseManager;

public interface IDatabaseManager
{
    bool Initialized { get; }

    event DatabaseCreated DatabaseCreated;

    Task<bool> Init(CancellationToken token);

    Task<ImmutableHashSet<string>> GetRootPaths(CancellationToken token);

    Task<ImmutableHashSet<string>> GetArchiveNamesForRoot(string rootDirectory, CancellationToken token);

    Task<string> GetActiveArchiveNameForRoot(string rootDirectory, CancellationToken token);

    Task<ImmutableHashSet<string>> GetFilesForArchiveName(string archiveToBuild,CancellationToken token);

    Task AddFileToArchive(string nameOfFileToAdd, string archiveFileName,CancellationToken token);

    Task RemoveFileFromArchiveRecord(string fileToRemove, string archiveFileName, CancellationToken token);

    Task RegisterNewRootPath(string rootPathToRegister, string archiveName, CancellationToken token, bool loadRootSection = false);

    Task<string> GetArchiveFileNameByFileEntryName(string fileName,CancellationToken token);

    Task UpdateSizeForArchive(long compressedSizeInBytes, string name, CancellationToken token);

    Task RegisterNewArchiveFile(string archiveFileName, string rootDirectory, CancellationToken token);

    Task RenameFile(string oldFile, string newFile, string archiveFileName, CancellationToken token);

    Task MoveAllFilesFromOldPathToNewPathForArchiveFile(string oldDirectory, string newDirectory, string archiveFileName, CancellationToken token);

    Task<bool> HasRoot(string rootPath, CancellationToken token);
    Task<bool> HasFile(string filePath, CancellationToken token);
}