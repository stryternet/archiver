using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Archiver.Exceptions;

namespace Archiver
{
    public class ArchiveFile(string name, string pathToArchiveFile, CompressionLevel compressionLevel)
    {
        private readonly string _pathToArchiveFile = pathToArchiveFile;

        private readonly string _pathToTemporaryArchiveFile = $"{pathToArchiveFile}.tmp";

        public string Name { get; } = name;

        public long CompressedSizeInBytes => GetArchiveFileSizeInBytes();

        private readonly CompressionLevel _compressionLevel = compressionLevel;

        private long GetArchiveFileSizeInBytes()
        {
            if (File.Exists(_pathToArchiveFile))
            {
                return new FileInfo(_pathToArchiveFile).Length;
            }

            return 0;
        }

        public void DeleteEntry(string entryName)
        {
            if (File.Exists(_pathToArchiveFile) == false)
            {
                throw new ZipArchiveNotFoundException(_pathToArchiveFile, "Tried to delete an entry from an archive that doesn't exist.");
            }

            // Open a read-only file stream to the permanent archive
            using (FileStream permanentArchive = new(_pathToArchiveFile, FileMode.Open))
            {
                // Create the zip archive instance from the archive file stream
                using ZipArchive readOnlyZip = new(permanentArchive, ZipArchiveMode.Read, leaveOpen: false);

                if (readOnlyZip.GetEntry(entryName) == null)
                {
                    // The entry cannot be found in the archive. This could be because it has already been removed.
                    // There's nothing more to be done except to leave.
                    return;
                }

                // Create a file stream that will make the temporary archive
                using (FileStream temporaryArchiveFileStream = new(_pathToTemporaryArchiveFile, FileMode.Create))
                {

                    // Create the 'writable' zip archive instance for the temporary archive file stream
                    using ZipArchive tempZipForWrite = new(temporaryArchiveFileStream, ZipArchiveMode.Create, leaveOpen: false);

                    // This needs to read each existing entry from the permanent archive into the temporary archive.
                    foreach (ZipArchiveEntry readOnlyEntry in readOnlyZip.Entries)
                    {

                        if (readOnlyEntry.FullName == entryName)
                        {
                            // This is the file to delete. Don't copy the entry over into the temporary archive.
                            continue;
                        }

                        // Create a 'new' entry in the temporary zip archive to store the entry that is being copied over
                        ZipArchiveEntry copyEntry = tempZipForWrite.CreateEntry(readOnlyEntry.FullName, _compressionLevel);

                        // Open a read-only file stream to the existing entry file
                        using (Stream sourceEntryStream = readOnlyEntry.Open())
                        {
                            // Copy the entry file to the temporary archive
                            using (Stream entryStream = copyEntry.Open())
                            {
                                sourceEntryStream.CopyTo(entryStream);
                            }
                        }
                    }
                }
            }


            MakeTempZipFilePermanent();
        }

        public void AddOrUpdateEntry(string entryName)
        {
            if (File.Exists(entryName) == false)
            {
                // The file no longer exists on disk. Maybe it was deleted, or it was a temporary file. Either way, nothing to do, so just leave.
                return;
            }
            // Open the file stream to the file that needs to be archived
            using (FileStream sourceFileStream = new(File.OpenHandle(entryName), FileAccess.Read))
            {

                if (File.Exists(_pathToArchiveFile) == false)
                {
                    CreateZipFile(_pathToArchiveFile);
                }
                // Open a read-only file stream to the permanent archive
                using (FileStream permanentArchive = new(_pathToArchiveFile, FileMode.Open))
                {

                    // Create the zip archive instance from the archive file stream
                    using ZipArchive readOnlyZip = new(permanentArchive, ZipArchiveMode.Read, leaveOpen: false);

                    // Create a file stream that will make the temporary archive
                    using (FileStream temporaryArchiveFileStream = new(_pathToTemporaryArchiveFile, FileMode.Create))
                    {

                        // Create the 'writable' zip archive instance for the temporary archive file stream
                        using ZipArchive tempZipForWrite = new(temporaryArchiveFileStream, ZipArchiveMode.Create, leaveOpen: false);

                        // This needs to read each existing entry from the permanent archive into the temporary archive.
                        foreach (ZipArchiveEntry readOnlyEntry in readOnlyZip.Entries)
                        {

                            if (readOnlyEntry.FullName == entryName)
                            {
                                // This is an update. Don't copy the entry over into the temporary archive. It will get added below.
                                continue;
                            }

                            // Create a 'new' entry in the temporary zip archive to store the entry that is being copied over
                            ZipArchiveEntry copyEntry = tempZipForWrite.CreateEntry(readOnlyEntry.FullName, _compressionLevel);

                            // Open a read-only file stream to the existing entry file
                            using (Stream sourceEntryStream = readOnlyEntry.Open())
                            {
                                // Copy the entry file to the temporary archive
                                using (Stream entryStream = copyEntry.Open())
                                {
                                    sourceEntryStream.CopyTo(entryStream);
                                }
                            }
                        }

                        if (File.Exists(entryName) == false)
                        {
                            // The file no longer exists on disk. Maybe it was deleted, or it was a temporary file. Either way, nothing to do, so just leave.
                            return;
                        }

                        ZipArchiveEntry entryForFileToBeArchived = tempZipForWrite.CreateEntry(entryName, _compressionLevel);

                        using (FileStream sourceEntryStream = new(entryName, FileMode.Open))
                        {
                            // Copy the entry file to the temporary archive
                            using (Stream entryStream = entryForFileToBeArchived.Open())
                            {
                                sourceEntryStream.CopyTo(entryStream);
                            }
                        }
                    }
                }
            }

            MakeTempZipFilePermanent();
        }

        public bool HasEntries()
        {
            if (File.Exists(_pathToArchiveFile) == false)
            {
                return false;
            }
            int entriesCount = 0;
            // Open a read-only file stream to the permanent archive
            using (FileStream permanentArchive = new(_pathToArchiveFile, FileMode.Open))
            {
                using ZipArchive readOnlyZip = new(permanentArchive, ZipArchiveMode.Read, leaveOpen: false);

                entriesCount = readOnlyZip.Entries.Count;
            }

            return entriesCount > 0;
        }

        public IEnumerable<string> GetEntries()
        {
            if (File.Exists(_pathToArchiveFile) == false)
            {
                return [];
            }

            using FileStream permanentArchive = new(_pathToArchiveFile, FileMode.Open);
            using ZipArchive readOnlyZip = new(permanentArchive, ZipArchiveMode.Read, leaveOpen: false);

            return readOnlyZip.Entries.Select(e => e.FullName);
        }

        private void MakeTempZipFilePermanent()
        {
            File.Move(_pathToTemporaryArchiveFile, _pathToArchiveFile, true);
            File.Delete(_pathToTemporaryArchiveFile);
        }

        private static void CreateZipFile(string pathToArchiveFile)
        {
            if (File.Exists(pathToArchiveFile))
            {
                return;
            }

            using FileStream fs = new(pathToArchiveFile, FileMode.Create);
            using ZipArchive zip = new(fs, ZipArchiveMode.Create);
            // Zip file created
        }
    }
}