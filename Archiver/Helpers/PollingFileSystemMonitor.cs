using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public enum PollingFileSystemEventType
{
    Created,
    Changed,
    Deleted
}

public class PollingFileSystemEventArgsCollection(PollingFileSystemEventType type, HashSet<string> files, string rootPath) : EventArgs
{
    public PollingFileSystemEventType ChangeType { get; } = type;
    public HashSet<string> Files { get; } = files;
    public string RootPath { get; } = rootPath;
}

public class PollingFileSystemEventArgs(PollingFileSystemEventType type, string fullFilePath, string relativePath)
{
    public PollingFileSystemEventType ChangeType { get; } = type;
    public string FullPath { get; } = fullFilePath;
    public string RelativePath { get; } = relativePath;
    public string Name { get; } = Path.GetFileName(fullFilePath);
}

public class PollingFileSystemMonitor(string directoryPath, ILogger logger, int pollingInterval = 5000, int stabilityTime = 60000)
{
    private readonly string _directoryPath = directoryPath;
    private readonly int _pollingInterval = pollingInterval;
    private readonly int _stabilityTime = stabilityTime;
    private readonly Dictionary<string, DateTime> _fileLastWriteTimes = new Dictionary<string, DateTime>();
    private readonly Dictionary<string, DateTime> _potentialModifications = new Dictionary<string, DateTime>();

    public event EventHandler<PollingFileSystemEventArgsCollection> Created;
    public event EventHandler<PollingFileSystemEventArgsCollection> Changed;
    public event EventHandler<PollingFileSystemEventArgsCollection> Deleted;

    private ILogger _logger = logger;

    public async Task StartAsync(CancellationToken cancellationToken = default)
    {
        await Task.Run(() => PollDirectoryAsync(cancellationToken), cancellationToken);
    }

    private async Task PollDirectoryAsync(CancellationToken cancellationToken)
    {
        EnumerationOptions opts = new()
        {
            IgnoreInaccessible = true,
            RecurseSubdirectories = true,
            BufferSize = 16000
        };

        HashSet<string> knownFiles = new(Directory.EnumerateFiles(_directoryPath, "*.*", opts));

        while (!cancellationToken.IsCancellationRequested)
        {
            _logger.LogInformation("Waiting {amount}ms before polling files for {dir}", _pollingInterval, _directoryPath);
            await Task.Delay(_pollingInterval, cancellationToken);

            _logger.LogInformation("Polling for files in the directory: {dir}", _directoryPath);

            HashSet<string> currentFiles = new HashSet<string>(Directory.EnumerateFiles(_directoryPath, "*.*", opts));
            _logger.LogInformation("Retrieved {num} files from {dir}", currentFiles.Count, _directoryPath);
            HashSet<string> createdFiles = currentFiles.Except(knownFiles).ToHashSet();
            HashSet<string> deletedFiles = knownFiles.Except(currentFiles).ToHashSet();
            _logger.LogInformation("Filtered created and deleted files from {dir}", _directoryPath);

            if (deletedFiles.Count != 0)
            {
                _logger.LogInformation("Found {number} deleted files. Emitting event.", deletedFiles.Count);
                Deleted?.Invoke(this, new PollingFileSystemEventArgsCollection(PollingFileSystemEventType.Deleted, deletedFiles, _directoryPath));
            }
            if (createdFiles.Count != 0)
            {
                _logger.LogInformation("Found {number} created files. Emitting event.", createdFiles.Count);
                Created?.Invoke(this, new PollingFileSystemEventArgsCollection(PollingFileSystemEventType.Created, createdFiles, _directoryPath));
            }

            HashSet<string> modifiedFiles = [];
            foreach (string file in currentFiles)
            {
                DateTime lastWriteTime = File.GetLastWriteTime(file);
                if (_fileLastWriteTimes.ContainsKey(file))
                {
                    if (lastWriteTime != _fileLastWriteTimes[file])
                    {
                        _logger.LogInformation("Detected a potential file modification for: {file}", file);
                        _potentialModifications[file] = DateTime.Now;
                    }
                    else if (_potentialModifications.ContainsKey(file) && (DateTime.Now - _potentialModifications[file]).TotalMilliseconds >= _stabilityTime)
                    {
                        _logger.LogInformation("The file modification has been stable for {stabilityTime}ms for: {file}", _stabilityTime, file);
                        modifiedFiles.Add(file);
                        _potentialModifications.Remove(file);
                    }
                }
                _fileLastWriteTimes[file] = lastWriteTime;
            }

            knownFiles = currentFiles;

            if (modifiedFiles.Count != 0)
            {
                _logger.LogInformation("Found {number} modded files. Emitting event.", modifiedFiles.Count);
                Changed?.Invoke(this, new PollingFileSystemEventArgsCollection(PollingFileSystemEventType.Changed, modifiedFiles, _directoryPath));
            }
        }
    }
}
