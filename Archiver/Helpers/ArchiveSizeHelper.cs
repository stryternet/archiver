using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Archiver.Helpers
{
    public class ArchiveSizeHelper(CompressionLevel compressionLevel)
    {

        private readonly Dictionary<string, long> _fileSizes = [];
        private readonly CompressionLevel _compressionLevel = compressionLevel;

        public void AddFile(string fullFilePath)
        {
            FileInfo f = new(fullFilePath);
            using PositionWrapperStream memoryStream = new(Stream.Null);
            using (ZipArchive archive = new(memoryStream, ZipArchiveMode.Create, true))
            {
                var entry = archive.CreateEntry(fullFilePath, _compressionLevel);
                using var entryStream = entry.Open();
                WriteZero(entryStream, f.Length);
            }

            _fileSizes[fullFilePath] = memoryStream.Length;
        }

        public long GetCompressedSize(string fileName)
        {
            if (_fileSizes.TryGetValue(fileName, out long size))
            {
                return size * 1024L;
            }
            else
            {
                throw new KeyNotFoundException("File not found");
            }
        }

        private static void WriteZero(Stream target, long count)
        {
            byte[] buffer = new byte[1024];
            while (count > 0)
            {
                target.Write(buffer, 0, (int)Math.Min(buffer.Length, count));
                count -= buffer.Length;
            }
        }
    }

    class PositionWrapperStream(Stream wrapped) : Stream
    {
        private readonly Stream _wrapped = wrapped;

        private int _pos = 0;

        public override bool CanSeek { get { return false; } }

        public override bool CanWrite { get { return true; } }

        public override long Position
        {
            get { return _pos; }
            set { throw new NotSupportedException(); }
        }

        public override bool CanRead => throw new NotImplementedException();

        public override long Length => Position;

        public override void Flush()
        {
            // Nothing to do!
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _pos += count;
            _wrapped.Write(buffer, offset, count);
        }
    }
}