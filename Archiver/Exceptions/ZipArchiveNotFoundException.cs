using System;

namespace Archiver.Exceptions
{
    public class ZipArchiveNotFoundException(string nameOrReference, string additionalDetail = "") : Exception($"Couldn't find a zip archive by the name or reference: {nameOrReference}. {additionalDetail}")
    {
        
    }
}