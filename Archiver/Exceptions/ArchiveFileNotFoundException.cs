using System;

namespace Archiver.Exceptions
{
    public class ArchiveFileNotFoundException(string expectedArchiveFileName, string additionalDetail) : Exception($"Couldn't find archive file by name or path {expectedArchiveFileName}. {additionalDetail}")
    {
    }
}