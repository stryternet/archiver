using System;
using System.Collections.Generic;
using System.Threading;

namespace Archiver.Utilities;

/// <summary>
/// A tool that acts like a gateway or a funnel around a segment of code that requires multi-threading protection.
/// This tool prevents multiple threads from gaining access to the protected code at the same time; only one
/// thread will be able to access the code.
/// </summary>
public class SyncDispatcher : IDisposable
{
    private static object _lock = new();
    private static Dictionary<object, SyncDispatcher> _container = [];

    private AutoResetEvent _syncEvent = new(true);

    private SyncDispatcher() { }

    private void Lock()
    {
        _syncEvent.WaitOne();
    }

    public void Dispose()
    {
        _syncEvent.Set();
        GC.SuppressFinalize(this);
    }

    public static SyncDispatcher Enter(object obj)
    {
        var objDispatcher = GetSyncDispatcher(obj);
        objDispatcher.Lock();

        return objDispatcher;
    }

    private static SyncDispatcher GetSyncDispatcher(object obj)
    {
        lock (_lock)
        {
            if (!_container.ContainsKey(obj))
            {
                _container.Add(obj, new SyncDispatcher());
            }

            return _container[obj];
        }
    }
}