using System.IO;

namespace Archiver.Utilities
{
    public static class ArchiveUtility
    {
        public static string BuildArchiveNameFromDirectoryToArchive(string directoryToArchive)
        {
            return Path.GetFileNameWithoutExtension(directoryToArchive);
        }
    }
}