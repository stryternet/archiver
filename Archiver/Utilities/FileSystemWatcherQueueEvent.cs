namespace Archiver
{
    public class FileSystemWatcherQueueEvent(PollingFileSystemEventArgs e)
    {
        public PollingFileSystemEventArgs Event { get; private init; } = e;
    }
}