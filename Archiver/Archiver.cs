using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Config;
using Archiver.Exceptions;
using Archiver.Helpers;
using Archiver.Managers.DatabaseManager;
using Archiver.Utilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Archiver
{
    public interface IArchiver
    {
        Task<bool> Init(CancellationToken token);

        Task OnCreate(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false);

        Task OnDelete(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false);

        Task OnChange(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false);

        //Task OnRename(RenamedEventArgs e, CancellationToken token, bool isForDirectory = false);
    }

    public class Archiver(ILogger<Archiver> logger, IDatabaseManager databaseManager, IOptions<ArchiverConfig> config, ArchiveSizeHelper archiveSizeHelper) : IArchiver
    {
        private readonly ILogger<Archiver> _logger = logger;

        private readonly IDatabaseManager _databaseManager = databaseManager;

        private readonly ArchiveSizeHelper _archiveSizeHelper = archiveSizeHelper;

        private readonly Dictionary<string, ArchiveManager> _archiveManagers = [];

        private ArchiverConfig _config = config.Value;
        private bool _initialized = false;

        private bool _doInitialScanForFiles = false;

        private HashSet<string> _fileTypesToIgnore = config.Value.FileTypesToIgnore.Select(ft => ft.ToLower()).ToHashSet();

        private HashSet<string> _filesToIgnore = config.Value.FilesToIgnore.Select(ft => ft.ToLower()).ToHashSet();

        public async Task<bool> Init(CancellationToken token)
        {
            _logger.LogInformation("Initializing the Archiver");
            _logger.LogInformation("Building new archives with size of {sizeLimitMb}MB", _config.SizeLimitMB);

            if (_config.StartFresh)
            {
                await WipeOutAllFiles(token);
            }

            _databaseManager.DatabaseCreated += () =>
                _config.RootPaths
                    .ToList()
                    .ForEach(async r =>
                    {
                        await _databaseManager.RegisterNewRootPath(r, $"{ArchiveUtility.BuildArchiveNameFromDirectoryToArchive(r)}.001.zip", token);
                        _doInitialScanForFiles = true;
                    });

            if (_databaseManager.Initialized == false)
            {
                await _databaseManager.Init(token);
                _logger.LogDebug("Finished initializing the database manager");
            }

            ImmutableHashSet<string> rootPaths = await _databaseManager.GetRootPaths(token);

            _logger.LogInformation("Checking to see if there are any new root paths added to the config or environment that have not been registered yet.");
            rootPaths = CheckAndAddMissingRootPathsFromConfig(rootPaths);

            foreach (string rootPath in rootPaths)
            {
                _logger.LogInformation("Registering the root path: {rootPath}", rootPath);
                await RegisterArchiveManagerForRootPath(rootPath, token);
            }

            if (_doInitialScanForFiles)
            {
                _logger.LogInformation("Doing an initial scan of files to add to the archive(s) beneath all root paths");
                Stopwatch stopwatch = new();
                stopwatch.Start();
                await PopulateArchives(token);
                _doInitialScanForFiles = false;
                stopwatch.Stop();

                bool useMin = stopwatch.Elapsed.TotalSeconds > 60;

                _logger.LogInformation("Archive population took {minutes} {minutesOrSeconds}", useMin ? stopwatch.Elapsed.TotalSeconds / 60 : stopwatch.Elapsed.TotalSeconds, useMin ? "minutes" : "seconds");
            }
            else
            {
                await RunIntegrityChecks(token);
            }

            return _initialized = true;
        }

        private async Task RunIntegrityChecks(CancellationToken token)
        {
            await RebuildDatabaseFileFromArchiveContentsIntegrityCheck(token);
            await RunUntrackedFilesIntegrityCheck(token);
        }

        private async Task RebuildDatabaseFileFromArchiveContentsIntegrityCheck(CancellationToken token)
        {
            ConcurrentQueue<KeyValuePair<string, ArchiveManager>> workQueue = new(_archiveManagers);


            List<Task> toProcessTasks = [];
            for (int i = 0; i < 3; i++)
            {
                Task t = RebuildDatabaseFileFromArchiveContentsIntegrityCheckWorker(workQueue, token);
                toProcessTasks.Add(t);
            }

            await Task.WhenAll(toProcessTasks);
        }

        private async Task RebuildDatabaseFileFromArchiveContentsIntegrityCheckWorker(ConcurrentQueue<KeyValuePair<string, ArchiveManager>> queueToProcess, CancellationToken token)
        {
            while (queueToProcess.IsEmpty == false && token.IsCancellationRequested == false)
            {
                if (queueToProcess.TryDequeue(out KeyValuePair<string, ArchiveManager> manager))
                {
                    await RebuildDatabaseFileFromArchiveContents(manager.Key, manager.Value, _logger, token);
                }
            }
        }

        private async Task RebuildDatabaseFileFromArchiveContents(string rootPath, ArchiveManager manager, ILogger<Archiver> logger, CancellationToken token)
        {
            await manager.RebuildDatabaseFileFromArchiveContents(token);
        }

        private async Task RunUntrackedFilesIntegrityCheck(CancellationToken token)
        {
            ConcurrentQueue<KeyValuePair<string, ArchiveManager>> workQueue = new(_archiveManagers);


            List<Task> toProcessTasks = [];
            for (int i = 0; i < 3; i++)
            {
                Task t = UntrackedFilesIntegrityCheckWorker(workQueue, token);
                toProcessTasks.Add(t);
            }

            await Task.WhenAll(toProcessTasks);
        }

        private async Task UntrackedFilesIntegrityCheckWorker(ConcurrentQueue<KeyValuePair<string, ArchiveManager>> queueToProcess, CancellationToken token)
        {
            while (queueToProcess.IsEmpty == false && token.IsCancellationRequested == false)
            {
                if (queueToProcess.TryDequeue(out KeyValuePair<string, ArchiveManager> manager))
                {
                    await CheckForUntrackedFilesUnderRootAndAddToArchive(manager.Key, manager.Value, _logger, token);
                }
            }
        }

        private async Task CheckForUntrackedFilesUnderRootAndAddToArchive(string rootPath, ArchiveManager manager, ILogger<Archiver> _logger, CancellationToken token)
        {
            _logger.LogInformation("Checking for untracked, unarchived files beneath the root path: {rootPath}", rootPath);

            void failedFilesHandler(Dictionary<string, Exception> failedFiles)
            {
                _logger.LogError("{filesCount} file(s) failed to be added to the archives of root path: {rootPath}", failedFiles.Count, rootPath);
                foreach (KeyValuePair<string, Exception> failedFile in failedFiles)
                {
                    _logger.LogError("The file ({failedFile}) failed due to reason: {reason}", failedFile.Key, failedFile.Value.Message);
                }
            }
            manager.FilesAddedFailure += failedFilesHandler;

            int totalFilesToBeAdded = 0;
            void addFilesHandler(int filesCount)
            {
                _logger.LogInformation("Adding {filesCount} file(s) to the archives of root path: {rootPath}", filesCount, rootPath);
                totalFilesToBeAdded = filesCount;
            }
            manager.AddFiles += addFilesHandler;

            void filesAddedHandler(int currentProgressCount)
            {
                int progressPercentage = (int)((double)currentProgressCount / (double)totalFilesToBeAdded * 100);

                _logger.LogInformation("Progress of files added: ({currentProgressCount} / {totalFilesToBeAdded} files added) {progressPercentage}% for root path {rootPath}",
                currentProgressCount, totalFilesToBeAdded, progressPercentage, rootPath);
            }
            manager.FilesAdded += filesAddedHandler;

            ImmutableHashSet<string> allFilesOnDiskBeneathRootPath = Directory.EnumerateFiles(rootPath, "*.*", SearchOption.AllDirectories).ToImmutableHashSet();

            HashSet<string> untrackedFilesToAdd = [];

            foreach (string filePath in allFilesOnDiskBeneathRootPath)
            {
                if (IsFilePathIgnorable(filePath))
                {
                    _logger.LogInformation("The file, {filePath}, has been configured to be ignored. Skipping.", filePath);
                    continue;
                }

                bool isFileTracked = await manager.IsTrackingFile(filePath, token);

                if (isFileTracked)
                {
                    continue;
                }

                untrackedFilesToAdd.Add(filePath);
            }

            _logger.LogInformation("Found {numberOfUntrackedFiles} file(s) that are not currently archived. Adding to the archive for root path: {rootPath}", untrackedFilesToAdd.Count, rootPath);

            await manager.AddFilesToArchive([.. untrackedFilesToAdd], token);

            manager.FilesAddedFailure -= failedFilesHandler;
            manager.AddFiles -= addFilesHandler;
            manager.FilesAdded -= filesAddedHandler;
        }

        private Task WipeOutAllFiles(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            foreach (string file in Directory.EnumerateFiles(_config.ArchiveStoragePath))
            {
                File.Delete(file);
            }

            return Task.CompletedTask;
        }

        private ImmutableHashSet<string> CheckAndAddMissingRootPathsFromConfig(ImmutableHashSet<string> rootPaths)
        {
            HashSet<string> newSet = [.. rootPaths];
            foreach (string configRootPath in _config.RootPaths)
            {
                if (newSet.Contains(configRootPath) == false)
                {
                    _logger.LogInformation("Found a new path from the config or environment to add to the list of paths to register: {configRootPath}", configRootPath);
                    newSet.Add(configRootPath);
                }
            }

            return [.. newSet];
        }

        private async Task PopulateArchives(CancellationToken token)
        {
            ConcurrentQueue<KeyValuePair<string, ArchiveManager>> workQueue = new(_archiveManagers);


            List<Task> toProcessTasks = [];
            for (int i = 0; i < 3; i++)
            {
                Task t = TaskWorker(workQueue, token);
                toProcessTasks.Add(t);
            }

            await Task.WhenAll(toProcessTasks);
        }

        private async Task TaskWorker(ConcurrentQueue<KeyValuePair<string, ArchiveManager>> queueToProcess, CancellationToken token)
        {
            while (queueToProcess.IsEmpty == false && token.IsCancellationRequested == false)
            {
                if (queueToProcess.TryDequeue(out KeyValuePair<string, ArchiveManager> manager))
                {
                    await PopulateArchive(manager.Key, manager.Value, _logger, token);
                }
            }
        }

        private static async Task PopulateArchive(string rootPath, ArchiveManager manager, ILogger<Archiver> _logger, CancellationToken token)
        {
            _logger.LogInformation("Populating archives for the root path: {rootPath}", rootPath);

            void failedFilesHandler(Dictionary<string, Exception> failedFiles)
            {
                _logger.LogError("{filesCount} file(s) failed to be added to the archives of root path: {rootPath}", failedFiles.Count, rootPath);
                foreach (KeyValuePair<string, Exception> failedFile in failedFiles)
                {
                    _logger.LogError("The file ({failedFile}) failed due to reason: {reason}", failedFile.Key, failedFile.Value.Message);
                }
            }
            manager.FilesAddedFailure += failedFilesHandler;

            int totalFilesToBeAdded = 0;
            void addFilesHandler(int filesCount)
            {
                _logger.LogInformation("Adding {filesCount} file(s) to the archives of root path: {rootPath}", filesCount, rootPath);
                totalFilesToBeAdded = filesCount;
            }
            manager.AddFiles += addFilesHandler;

            void filesAddedHandler(int currentProgressCount)
            {
                int progressPercentage = (int)((double)currentProgressCount / (double)totalFilesToBeAdded * 100);

                _logger.LogInformation("Progress of files added: ({currentProgressCount} / {totalFilesToBeAdded} files added) {progressPercentage}% for root path {rootPath}",
                currentProgressCount, totalFilesToBeAdded, progressPercentage, rootPath);
            }
            manager.FilesAdded += filesAddedHandler;
            await manager.AddFilesToArchive(Directory.EnumerateFiles(rootPath, "*.*", SearchOption.AllDirectories).ToImmutableHashSet(), token);

            manager.FilesAddedFailure -= failedFilesHandler;
            manager.AddFiles -= addFilesHandler;
            manager.FilesAdded -= filesAddedHandler;
        }

        private async Task RegisterArchiveManagerForRootPath(string rootPath, CancellationToken token)
        {
            if (_archiveManagers.ContainsKey(rootPath) == false)
            {
                bool shouldPopulateArchiveImmediately = false;
                if (await _databaseManager.HasRoot(rootPath, token) == false)
                {
                    await _databaseManager.RegisterNewRootPath(rootPath, $"{ArchiveUtility.BuildArchiveNameFromDirectoryToArchive(rootPath)}.001.zip", token, loadRootSection: true);
                    shouldPopulateArchiveImmediately = true;
                }
                ArchiveManager archiveManager = new(rootPath, _config.ArchiveStoragePath, _config.SizeLimitMB, _config.CompressionLevel, _databaseManager, _archiveSizeHelper, _logger);

                await archiveManager.Init(token);

                _archiveManagers.Add(rootPath, archiveManager);

                if (shouldPopulateArchiveImmediately)
                {
                    _logger.LogInformation("Populating the archive(s) for the new root path: {rootPath}", rootPath);
                    await PopulateArchive(rootPath, archiveManager, _logger, token);
                }
            }
        }

        public async Task OnCreate(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false)
        {
            using (SyncDispatcher.Enter(this))
            {
                if (_initialized == false)
                    return;
                _logger.LogInformation("File created: {name}", e.FullPath);

                string rootPath;
                if (isForDirectory)
                {
                    if (TryGetFilesFromDirectory(e.FullPath, out IEnumerable<string> filesToCreate))
                    {
                        foreach (string fileToCreate in filesToCreate)
                        {
                            FormatNameAndFullPathFromEvent(e, out _, out rootPath);
                            await AddOrUpdateFileForRoot(fileToCreate.Replace(rootPath, ""), rootPath, token);
                        }
                    }
                    return;
                }

                FormatNameAndFullPathFromEvent(e, out string formattedName, out rootPath);

                CheckIfTypeIsIgnorable(formattedName);
                CheckIfFileIsIgnorable(formattedName);
                await AddOrUpdateFileForRoot(formattedName, rootPath, token);

                return;
            }
        }

        public async Task OnDelete(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false)
        {
            using (SyncDispatcher.Enter(this))
            {
                if (_initialized == false)
                    return;
                _logger.LogInformation("File deleted: {name}", e.FullPath);

                FormatNameAndFullPathFromEvent(e, out string formattedName, out string rootPath);

                CheckIfTypeIsIgnorable(formattedName);
                CheckIfFileIsIgnorable(formattedName);
                await RemoveFileForRoot(formattedName, rootPath, token);



                return;
            }
        }

        public async Task OnChange(PollingFileSystemEventArgs e, CancellationToken token, bool isForDirectory = false)
        {
            using (SyncDispatcher.Enter(this))
            {
                if (_initialized == false)
                    return;
                _logger.LogInformation("File changed: {name}", e.FullPath);

                if (isForDirectory || Path.Exists(e.FullPath) == false || File.Exists(e.FullPath) == false)
                {
                    return;
                }

                FormatNameAndFullPathFromEvent(e, out string formattedName, out string rootPath);

                CheckIfTypeIsIgnorable(formattedName);
                CheckIfFileIsIgnorable(formattedName);

                await AddOrUpdateFileForRoot(formattedName, rootPath, token);

                return;
            }
        }

        private bool IsFilePathIgnorable(string file)
        {
            try
            {
                CheckIfFileIsIgnorable(file);
                CheckIfTypeIsIgnorable(file);
            }
            catch (IgnorableFileTypeException)
            {
                return true;
            }

            return false;
        }

        private void CheckIfTypeIsIgnorable(string file)
        {
            string fileExtension = Path.GetExtension(file);
            if (_fileTypesToIgnore.Contains(fileExtension.ToLower()))
            {
                _logger.LogInformation("The file extension, {fileExtension}, for the file {previousName} is configured to be ignored and will not be processed.", fileExtension, file);
                throw new IgnorableFileTypeException();
            }
        }

        private void CheckIfFileIsIgnorable(string file)
        {
            string fileNameWithoutExt = Path.GetFileNameWithoutExtension(file);
            string fileNameWithExt = Path.GetFileName(file);
            if (_filesToIgnore.Contains(fileNameWithoutExt.ToLower()) || _filesToIgnore.Contains(fileNameWithExt.ToLower()))
            {
                _logger.LogInformation("The file, {file}, is configured to be ignored and will not be processed.", file);
                throw new IgnorableFileTypeException();
            }
        }

        private async Task RemoveFileForRoot(string fileName, string rootPath, CancellationToken token)
        {
            ArchiveManager am = _archiveManagers[rootPath];

            await am.RemoveFileFromArchive(rootPath + fileName, token);
        }

        private async Task AddOrUpdateFileForRoot(string fileName, string rootPath, CancellationToken token)
        {
            if (_archiveManagers.ContainsKey(rootPath) == false)
            {
                _logger.LogError("Could not find archive manager for root path: {rootPath}", rootPath);
                _logger.LogError("Archive managers in dictionary: {}", string.Join('\n', _archiveManagers.Keys));
                throw new ArchiveManagerNotFoundException();
            }
            ArchiveManager am = _archiveManagers[rootPath];

            await am.AddOrUpdateFileInArchive(rootPath + fileName, token);
        }

        private static void FormatNameAndFullPathFromEvent(PollingFileSystemEventArgs e, out string formattedName, out string rootPath)
        {
            formattedName = $@"{Path.DirectorySeparatorChar}{e.RelativePath}";
            rootPath = e.FullPath.Replace(e.RelativePath, "").TrimEnd(Path.DirectorySeparatorChar);
        }

        private static void FormatPreviousNameAndNewNameAndFullPathFromEvent(RenamedEventArgs e, out string previousName, out string newName, out string rootPath)
        {
            previousName = $@"{Path.DirectorySeparatorChar}{e.OldName}";
            newName = $@"{Path.DirectorySeparatorChar}{e.Name}";
            rootPath = e.FullPath.Replace(e.Name, "").TrimEnd(Path.DirectorySeparatorChar);
        }

        public static bool TryGetFilesFromDirectory(string path, out IEnumerable<string> filesBeneathDirectory)
        {
            if (Directory.Exists(path) == false)
            {
                filesBeneathDirectory = [];
                return false;
            }
            return (filesBeneathDirectory = Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories)).Any();
        }
    }
}