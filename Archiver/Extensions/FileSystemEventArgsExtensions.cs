using System.IO;

namespace Archiver.Extensions
{
    public static class PollingFileSystemEventArgsExtensions
    { 

        public static string ToHashKey(this PollingFileSystemEventArgs e)
        {
            return $"{e.ChangeType}::{Path.Combine(e.RelativePath, e.Name)}::{e.FullPath}";
        }
    }
}