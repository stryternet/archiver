using System.IO.Compression;

namespace Archiver.Config
{
    public class ArchiverConfig
    {
        public string[] RootPaths { get; set; }

        public string ArchiveStoragePath { get; set; }

        public int SizeLimitMB { get; set; }

        public string[] FileTypesToIgnore { get; set; }

        public string[] FilesToIgnore { get; set; }

        public bool StartFresh { get; set; }

        public CompressionLevel CompressionLevel { get; set; }
    }
}