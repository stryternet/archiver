namespace Archiver.Config
{
    public class StorageConfig
    {
        public string AccountId { get; set; }
        public string ApplicationKeyEnc { get; set; }
        public string BucketId { get; set; }
    }
}